MODULE HostDates;

  IMPORT Dates, Strings, time := PosixCtime;

  (* add localization? *)

  TYPE
    Hook = POINTER TO RECORD (Dates.Hook) END;

  VAR
    day: ARRAY 7, 10 OF CHAR;
    month: ARRAY 12, 10 OF CHAR;

  PROCEDURE (h: Hook) GetTime (OUT d: Dates.Date; OUT t: Dates.Time);
    VAR epoch: time.time_t; tm: time.struct_tm; ptm: time.Pstruct_tm;
  BEGIN
    epoch := time.time(NIL);
    ptm := time.localtime_r(epoch, tm);
    IF ptm # NIL THEN
      d.year := tm.tm_year + 1900;
      d.month := tm.tm_mon + 1;
      d.day := tm.tm_mday;
      t.hour := tm.tm_hour;
      t.minute := tm.tm_min;
      t.second := tm.tm_sec
    ELSE
      d.year := 0;
      d.month := 0;
      d.day := 0;
      t.hour := 0;
      t.minute := 0;
      t.second := 0
    END
  END GetTime;

  PROCEDURE (h: Hook) GetUTCTime (OUT d: Dates.Date; OUT t: Dates.Time);
    VAR epoch: time.time_t; tm: time.struct_tm; ptm: time.Pstruct_tm;
  BEGIN
    epoch := time.time(NIL);
    ptm := time.gmtime_r(epoch, tm);
    IF ptm # NIL THEN
      d.year := tm.tm_year + 1900;
      d.month := tm.tm_mon + 1;
      d.day := tm.tm_mday;
      t.hour := tm.tm_hour;
      t.minute := tm.tm_min;
      t.second := tm.tm_sec
    ELSE
      d.year := 0;
      d.month := 0;
      d.day := 0;
      t.hour := 0;
      t.minute := 0;
      t.second := 0
    END
  END GetUTCTime;

  PROCEDURE (h: Hook) GetUTCBias (OUT bias: INTEGER);
  BEGIN
    time.tzset;
    (* !!! fix Dev2 *)
    (* bias := time.timezone DIV 60 *)
    bias := 0;
  END GetUTCBias;

  PROCEDURE (h: Hook) DateToString (d: Dates.Date; format: INTEGER; OUT str: ARRAY OF CHAR);
    VAR s: ARRAY 20 OF CHAR;

    PROCEDURE Copy (IN s: ARRAY OF CHAR; n: INTEGER; OUT str: ARRAY OF CHAR);
      VAR i: INTEGER;
    BEGIN
      FOR i := 0 TO n - 1 DO
        str[i] := s[i]
      END;
      str[i] := 0X
    END Copy;

  BEGIN
    CASE format OF
    | Dates.short:
        str[0] := CHR(d.day DIV 10 MOD 10 + ORD("0"));
        str[1] := CHR(d.day MOD 10 + ORD("0"));
        str[2] := "/";
        str[3] := CHR(d.month DIV 10 MOD 10 + ORD("0"));
        str[4] := CHR(d.month MOD 10 + ORD("0"));
        str[5] := "/";
        str[6] := CHR(d.year DIV 10 MOD 10 + ORD("0"));
        str[7] := CHR(d.year MOD 10 + ORD("0"));
        str[8] := 0X
    | Dates.abbreviated:
        Copy(day[d.day - 1], 3, str);
        Copy(month[d.month - 1], 3, s); str := str + ", " + s;
        Strings.IntToString(d.year, s); str := str + ", " + s
    | Dates.long:
        str := day[d.day - 1] + ", " + month[d.month - 1];
        Strings.IntToString(d.year, s); str := str + ", " + s
    | Dates.plainAbbreviated:
        Copy(month[d.month - 1], 3, str);
        Strings.IntToString(d.day, s); str := str + " " + s;
        Strings.IntToString(d.year, s); str := str + ", " + s
    | Dates.plainLong:
        Strings.IntToString(d.day, s); str := month[d.month - 1] + " " + s;
        Strings.IntToString(d.year, s); str := str + ", " + s
    END
  END DateToString;

  PROCEDURE (h: Hook) TimeToString (t: Dates.Time; OUT str: ARRAY OF CHAR);
    VAR s: ARRAY 12 OF CHAR;
  BEGIN
    Strings.IntToString(t.hour, str);
    Strings.IntToString(t.minute, s); str := str + ":" + s;
    Strings.IntToString(t.second, s); str := str + ":" + s;
  END TimeToString;

  PROCEDURE Init;
    VAR h: Hook;
  BEGIN
    day[0] := "Monday";
    day[1] := "Tuesday";
    day[2] := "Wednesday";
    day[3] := "Thursday";
    day[4] := "Friday";
    day[5] := "Saturday";
    day[6] := "Sunday";
    month[0] := "January";
    month[1] := "February";
    month[2] := "March";
    month[3] := "April";
    month[4] := "May";
    month[5] := "June";
    month[6] := "July";
    month[7] := "August";
    month[8] := "September";
    month[9] := "October";
    month[10] := "November";
    month[11] := "December";
    NEW(h); Dates.SetHook(h)
  END Init;

BEGIN
  Init
END HostDates.
