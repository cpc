MODULE HostConsole;

  IMPORT S := SYSTEM, Console, HostLang, unistd := PosixCunistd;

  TYPE
    Directory = POINTER TO RECORD (Console.Directory) END;

  PROCEDURE (d: Directory) WriteChar (ch: CHAR);
    VAR res: INTEGER; s: ARRAY 2 OF CHAR; ss: ARRAY 12 OF SHORTCHAR;
  BEGIN
    s[0] := ch; s[1] := 0X;
    HostLang.StringToHost(s, ss, HostLang.replace, res);
    ASSERT(res = 0, 100);
    res := unistd.write(1, S.ADR(ss[0]), LEN(ss$));
    res := unistd.fsync(1)
  END WriteChar;

  PROCEDURE (d: Directory) WriteString (IN s: ARRAY OF CHAR);
    VAR ss: POINTER TO ARRAY OF SHORTCHAR; res: INTEGER;
  BEGIN
    NEW(ss, LEN(s$) * 4 + 1);
    HostLang.StringToHost(s, ss, HostLang.replace, res);
    ASSERT(res = 0, 100);
    res := unistd.write(1, S.ADR(ss[0]), LEN(ss$));
    res := unistd.fsync(1)
  END WriteString;

  PROCEDURE (d: Directory) WriteLn;
  BEGIN
    d.WriteChar(0AX)
  END WriteLn;

  PROCEDURE Init;
    VAR d: Directory;
  BEGIN
    NEW(d); Console.SetDir(d)
  END Init;

BEGIN
  Init
END HostConsole.
