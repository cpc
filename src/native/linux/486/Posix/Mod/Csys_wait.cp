MODULE PosixCsys_wait ['libc.so.6'];

  (* generated by genposix.sh, do not modify *)

  IMPORT SYSTEM, PosixCtypes, PosixCsys_types, PosixCsignal;

  TYPE
    char* = PosixCtypes.char;
    signed_char* = PosixCtypes.signed_char;
    unsigned_char* = PosixCtypes.unsigned_char;
    short* = PosixCtypes.short;
    short_int* = PosixCtypes.short_int;
    signed_short* = PosixCtypes.signed_short;
    signed_short_int* = PosixCtypes.signed_short_int;
    unsigned_short* = PosixCtypes.unsigned_short;
    unsigned_short_int* = PosixCtypes.unsigned_short_int;
    int* = PosixCtypes.int;
    signed* = PosixCtypes.signed;
    signed_int* = PosixCtypes.signed_int;
    unsigned* = PosixCtypes.unsigned;
    unsigned_int* = PosixCtypes.unsigned_int;
    long* = PosixCtypes.long;
    long_int* = PosixCtypes.long_int;
    signed_long* = PosixCtypes.signed_long;
    signed_long_int* = PosixCtypes.signed_long_int;
    unsigned_long* = PosixCtypes.unsigned_long;
    unsigned_long_int* = PosixCtypes.unsigned_long_int;
    long_long* = PosixCtypes.long_long;
    long_long_int* = PosixCtypes.long_long_int;
    signed_long_long* = PosixCtypes.signed_long_long;
    signed_long_long_int* = PosixCtypes.signed_long_long_int;
    unsigned_long_long* = PosixCtypes.unsigned_long_long;
    unsigned_long_long_int* = PosixCtypes.unsigned_long_long_int;
    float* = PosixCtypes.float;
    double* = PosixCtypes.double;
    long_double* = PosixCtypes.long_double;

  CONST
    WCONTINUED* = 8;
    WNOHANG* = 1;
    WUNTRACED* = 2;

  CONST
    WEXITED* = 4;
    WNOWAIT* = 16777216;
    WSTOPPED* = 2;

  TYPE
    idtype_t* = INTEGER;

  CONST
    P_ALL* = 0;
    P_PGID* = 2;
    P_PID* = 1;

  TYPE
    id_t* = PosixCsys_types.id_t;
    pid_t* = PosixCsys_types.pid_t;

  TYPE
    siginfo_t* = PosixCsignal.siginfo_t;

  PROCEDURE [ccall] wait* (VAR [nil] wstatus: int): pid_t;
  PROCEDURE [ccall] waitid* (idtype: idtype_t; id: id_t; VAR [nil] infop: siginfo_t; options: int): int;
  PROCEDURE [ccall] waitpid* (pid: pid_t; VAR [nil] wstatus: int; options: int): pid_t;

END PosixCsys_wait.
