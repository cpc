MODULE DswProcs;

  (* todo: pipe control, set cwd, set env vars, get program path from name *)

  IMPORT Kernel;

  TYPE
    String* = POINTER TO ARRAY OF CHAR;

    Directory* = POINTER TO ABSTRACT RECORD END;

    Process* = POINTER TO ABSTRACT RECORD END;

  VAR
    dir-, stdDir-: Directory;

  PROCEDURE (d: Directory) New* (): Process, NEW, ABSTRACT;
  PROCEDURE (d: Directory) GetPath* (IN name: ARRAY OF CHAR): String, NEW, ABSTRACT;

  PROCEDURE (p: Process) Program* (IN exe: ARRAY OF CHAR), NEW, ABSTRACT;
  PROCEDURE (p: Process) PutParam* (IN par: ARRAY OF CHAR), NEW, ABSTRACT;
  PROCEDURE (p: Process) Execute* (OUT ok: BOOLEAN), NEW, ABSTRACT;

  PROCEDURE (p: Process) Terminate* (OUT ok: BOOLEAN), NEW, ABSTRACT;
  PROCEDURE (p: Process) IsTerminated* (): BOOLEAN, NEW, ABSTRACT;
  PROCEDURE (p: Process) Wait*, NEW, ABSTRACT;
  PROCEDURE (p: Process) Result* (): INTEGER, NEW, ABSTRACT;

  PROCEDURE SetDir* (d: Directory);
  BEGIN
    ASSERT(d # NIL, 20);
    dir := d;
    IF stdDir = NIL THEN stdDir := d END
  END SetDir;

END DswProcs.
