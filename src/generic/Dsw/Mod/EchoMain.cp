MODULE DswEchoMain;

  (* encoding converter test *)

  IMPORT Kernel, HostLang, Console, Strings;

  PROCEDURE Init;
    CONST maxBuf = 256;
    VAR i, j, res: INTEGER; p, x: ARRAY maxBuf OF CHAR;
  BEGIN
    IF Kernel.trapCount # 0 THEN Kernel.Quit(1) END;
    i := 1;
    WHILE i < Kernel.argc DO
      HostLang.HostToString(Kernel.argv[i]$, p, TRUE, res);
      Console.WriteStr(p);
      IF res # 0 THEN
        Console.WriteLn;
        Console.WriteStr("Error: "); Console.WriteChar(CHR(ORD("0") + res MOD 10));
        Console.WriteLn;
        Kernel.Quit(1)
      END;
      INC(i)
    END;
    IF i > 1 THEN Console.WriteLn END;

    i := 1;
    WHILE i < Kernel.argc DO
      HostLang.HostToString(Kernel.argv[i]$, p, TRUE, res);
      j := 0;
      WHILE p[j] # 0X DO
        Strings.IntToStringForm(ORD(p[j]), 16, 4, "0", FALSE, x);
        Console.WriteStr(x);
        Console.WriteStr("  ");
        INC(j)
      END;
      INC(i)
    END;
    IF i > 1 THEN Console.WriteLn END;
    Kernel.Quit(0)
  END Init;

BEGIN
  Kernel.Start(Init)
END DswEchoMain.
