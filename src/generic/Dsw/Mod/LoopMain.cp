MODULE DswLoopMain;

  (* file locking test *)

  IMPORT Kernel, Files, Console, Strings, Log;

  VAR
    mode: BOOLEAN;

  PROCEDURE Init;
    VAR i: INTEGER; s: ARRAY 16 OF CHAR; f: Files.File; loc: Files.Locator;
  BEGIN
    IF (Kernel.trapCount > 0) & (Kernel.err # 128) THEN
      IF Kernel.err = 200 THEN Console.WriteStr("Keyboard Interrupt")
      ELSE Console.WriteStr("Trap "); Strings.IntToString(Kernel.err, s); Console.WriteStr(s)
      END;
      Console.WriteLn;
      Kernel.Quit(1)
    END;
    i := 1;
    mode := Files.shared;
    loc := Files.dir.This("");
    WHILE i < Kernel.argc DO
      IF Kernel.argv[i]$ = "-" THEN mode := Files.shared
      ELSIF Kernel.argv[i]$ = "+" THEN mode := Files.exclusive
      ELSIF Kernel.argv[i]$ = "!" THEN
        IF f # NIL THEN f.Close; f := NIL END
      ELSE
        f := Files.dir.Old(loc, Kernel.argv[i]$, mode);
        Log.Int(i); Log.String(": ");
        IF f = NIL THEN Log.String("~open")
        ELSE Log.String("ok")
        END;
        Log.Int(loc.res);
        Log.Ln
      END;
      INC(i)
    END;
    LOOP END;
    Kernel.Quit(0)
  END Init;

BEGIN
  Kernel.Start(Init)
END DswLoopMain.
