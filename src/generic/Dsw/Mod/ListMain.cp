MODULE DswListMain;

  (* file list test *)

  IMPORT Kernel, HostFiles, Files, Console, Strings;

  PROCEDURE Init;
    VAR i, res: INTEGER; loc: Files.Locator; f: Files.FileInfo; d: Files.LocInfo; s: ARRAY 20 OF CHAR;
  BEGIN
    IF Kernel.trapCount > 0 THEN Kernel.Quit(1) END;
    HostFiles.SetRootDir(".");
    i := 1; res := 0;
    WHILE i < Kernel.argc DO
      loc := Files.dir.This(Kernel.argv[i]$);
      d := Files.dir.LocList(loc);
      IF loc.res = 0 THEN
        WHILE d # NIL DO
          Console.WriteStr(d.name + "/");
          Console.WriteLn;
          d := d.next
        END;
        f := Files.dir.FileList(loc);
        IF loc.res = 0 THEN
          WHILE f # NIL DO
            Console.WriteStr(f.name);
            Console.WriteLn;
            f := f.next
          END
        END
      END;
      IF loc.res # 0 THEN
        Console.WriteStr(Kernel.argv[i] + ': error ');
        Strings.IntToString(loc.res, s);
        Console.WriteStr(s);
        Console.WriteLn;
        INC(res);
      END;
      INC(i)
    END;
    IF res = 0 THEN Kernel.Quit(0)
    ELSIF res = 1 THEN Kernel.Quit(1)
    ELSE Kernel.Quit(2)
    END
  END Init;

BEGIN
  Kernel.Start(Init)
END DswListMain.
