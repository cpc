MODULE PosixCunistd ['unistd.h'];

  (* generated by genposix.sh, do not modify *)

  IMPORT SYSTEM, PosixCtypes, PosixCsys_types;

  TYPE
    char* = PosixCtypes.char;
    signed_char* = PosixCtypes.signed_char;
    unsigned_char* = PosixCtypes.unsigned_char;
    short* = PosixCtypes.short;
    short_int* = PosixCtypes.short_int;
    signed_short* = PosixCtypes.signed_short;
    signed_short_int* = PosixCtypes.signed_short_int;
    unsigned_short* = PosixCtypes.unsigned_short;
    unsigned_short_int* = PosixCtypes.unsigned_short_int;
    int* = PosixCtypes.int;
    signed* = PosixCtypes.signed;
    signed_int* = PosixCtypes.signed_int;
    unsigned* = PosixCtypes.unsigned;
    unsigned_int* = PosixCtypes.unsigned_int;
    long* = PosixCtypes.long;
    long_int* = PosixCtypes.long_int;
    signed_long* = PosixCtypes.signed_long;
    signed_long_int* = PosixCtypes.signed_long_int;
    unsigned_long* = PosixCtypes.unsigned_long;
    unsigned_long_int* = PosixCtypes.unsigned_long_int;
    long_long* = PosixCtypes.long_long;
    long_long_int* = PosixCtypes.long_long_int;
    signed_long_long* = PosixCtypes.signed_long_long;
    signed_long_long_int* = PosixCtypes.signed_long_long_int;
    unsigned_long_long* = PosixCtypes.unsigned_long_long;
    unsigned_long_long_int* = PosixCtypes.unsigned_long_long_int;
    float* = PosixCtypes.float;
    double* = PosixCtypes.double;
    long_double* = PosixCtypes.long_double;

  CONST
    _POSIX_VERSION* = 200112;
    _POSIX2_VERSION* = 200112;
    _XOPEN_VERSION* = 600;

  CONST
    _POSIX_ADVISORY_INFO* = -1;
    _POSIX_ASYNCHRONOUS_IO* = -1;
    _POSIX_BARRIERS* = -1;
    _POSIX_CHOWN_RESTRICTED* = 200112;
    _POSIX_CLOCK_SELECTION* = -1;
    _POSIX_CPUTIME* = -1;
    _POSIX_FSYNC* = 200112;
    _POSIX_IPV6* = 200112;
    _POSIX_JOB_CONTROL* = 200112;
    _POSIX_MAPPED_FILES* = 200112;
    _POSIX_MEMLOCK* = -1;
    _POSIX_MEMLOCK_RANGE* = -1;
    _POSIX_MEMORY_PROTECTION* = 200112;
    _POSIX_MESSAGE_PASSING* = -1;
    _POSIX_MONOTONIC_CLOCK* = -1;
    _POSIX_NO_TRUNC* = 200112;
    _POSIX_PRIORITIZED_IO* = -1;
    _POSIX_PRIORITY_SCHEDULING* = -1;
    _POSIX_RAW_SOCKETS* = -1;
    _POSIX_READER_WRITER_LOCKS* = 200112;
    _POSIX_REALTIME_SIGNALS* = -1;
    _POSIX_REGEXP* = 200112;
    _POSIX_SAVED_IDS* = 200112;
    _POSIX_SEMAPHORES* = -1;
    _POSIX_SHARED_MEMORY_OBJECTS* = -1;
    _POSIX_SHELL* = 200112;
    _POSIX_SPAWN* = -1;
    _POSIX_SPIN_LOCKS* = -1;
    _POSIX_SPORADIC_SERVER* = -1;
    _POSIX_SYNCHRONIZED_IO* = -1;
    _POSIX_THREAD_ATTR_STACKADDR* = 200112;
    _POSIX_THREAD_ATTR_STACKSIZE* = 200112;
    _POSIX_THREAD_CPUTIME* = -1;
    _POSIX_THREAD_PRIO_INHERIT* = -1;
    _POSIX_THREAD_PRIO_PROTECT* = -1;
    _POSIX_THREAD_PRIORITY_SCHEDULING* = -1;
    _POSIX_THREAD_PROCESS_SHARED* = 200112;
    _POSIX_THREAD_ROBUST_PRIO_INHERIT* = -1;
    _POSIX_THREAD_ROBUST_PRIO_PROTECT* = -1;
    _POSIX_THREAD_SAFE_FUNCTIONS* = 200112;
    _POSIX_THREAD_SPORADIC_SERVER* = -1;
    _POSIX_THREADS* = 200112;
    _POSIX_TIMEOUTS* = -1;
    _POSIX_TIMERS* = -1;
    _POSIX_TRACE* = -1;
    _POSIX_TRACE_EVENT_FILTER* = -1;
    _POSIX_TRACE_INHERIT* = -1;
    _POSIX_TRACE_LOG* = -1;
    _POSIX_TYPED_MEMORY_OBJECTS* = -1;
    _POSIX_V6_ILP32_OFF32* = -1;
    _POSIX_V6_ILP32_OFFBIG* = -1;
    _POSIX_V6_LP64_OFF64* = -1;
    _POSIX_V6_LPBIG_OFFBIG* = -1;
    _POSIX_V7_ILP32_OFF32* = -1;
    _POSIX_V7_ILP32_OFFBIG* = -1;
    _POSIX_V7_LP64_OFF64* = -1;
    _POSIX_V7_LPBIG_OFFBIG* = -1;
    _POSIX2_C_BIND* = 200112;
    _POSIX2_C_DEV* = 200112;
    _POSIX2_CHAR_TERM* = 200112;
    _POSIX2_FORT_DEV* = -1;
    _POSIX2_FORT_RUN* = 200112;
    _POSIX2_LOCALEDEF* = 200112;
    _POSIX2_PBS* = -1;
    _POSIX2_PBS_ACCOUNTING* = -1;
    _POSIX2_PBS_CHECKPOINT* = -1;
    _POSIX2_PBS_LOCATE* = -1;
    _POSIX2_PBS_MESSAGE* = -1;
    _POSIX2_PBS_TRACK* = -1;
    _POSIX2_SW_DEV* = 200112;
    _POSIX2_UPE* = 200112;
    _XOPEN_CRYPT* = 1;
    _XOPEN_ENH_I18N* = 1;
    _XOPEN_REALTIME* = -1;
    _XOPEN_REALTIME_THREADS* = -1;
    _XOPEN_SHM* = 1;
    _XOPEN_STREAMS* = -1;
    _XOPEN_UNIX* = 1;
    _XOPEN_UUCP* = -1;

  CONST
    _POSIX_ASYNC_IO* = -1;
    _POSIX_PRIO_IO* = -1;
    _POSIX_SYNC_IO* = -1;
    _POSIX_TIMESTAMP_RESOLUTION* = -1;
    _POSIX2_SYMLINKS* = -1;

  CONST
    F_OK* = 0;
    R_OK* = 4;
    W_OK* = 2;
    X_OK* = 1;

  CONST
    _CS_PATH* = 1;
    _CS_POSIX_V7_ILP32_OFF32_CFLAGS* = -1;
    _CS_POSIX_V7_ILP32_OFF32_LDFLAGS* = -1;
    _CS_POSIX_V7_ILP32_OFF32_LIBS* = -1;
    _CS_POSIX_V7_ILP32_OFFBIG_CFLAGS* = -1;
    _CS_POSIX_V7_ILP32_OFFBIG_LDFLAGS* = -1;
    _CS_POSIX_V7_ILP32_OFFBIG_LIBS* = -1;
    _CS_POSIX_V7_LP64_OFF64_CFLAGS* = -1;
    _CS_POSIX_V7_LP64_OFF64_LDFLAGS* = -1;
    _CS_POSIX_V7_LP64_OFF64_LIBS* = -1;
    _CS_POSIX_V7_LPBIG_OFFBIG_CFLAGS* = -1;
    _CS_POSIX_V7_LPBIG_OFFBIG_LDFLAGS* = -1;
    _CS_POSIX_V7_LPBIG_OFFBIG_LIBS* = -1;
    _CS_POSIX_V7_THREADS_CFLAGS* = -1;
    _CS_POSIX_V7_THREADS_LDFLAGS* = -1;
    _CS_POSIX_V7_WIDTH_RESTRICTED_ENVS* = -1;
    _CS_V7_ENV* = -1;
    _CS_POSIX_V6_ILP32_OFF32_CFLAGS* = 2;
    _CS_POSIX_V6_ILP32_OFF32_LDFLAGS* = 3;
    _CS_POSIX_V6_ILP32_OFF32_LIBS* = 4;
    _CS_POSIX_V6_ILP32_OFFBIG_CFLAGS* = 5;
    _CS_POSIX_V6_ILP32_OFFBIG_LDFLAGS* = 6;
    _CS_POSIX_V6_ILP32_OFFBIG_LIBS* = 7;
    _CS_POSIX_V6_LP64_OFF64_CFLAGS* = 8;
    _CS_POSIX_V6_LP64_OFF64_LDFLAGS* = 9;
    _CS_POSIX_V6_LP64_OFF64_LIBS* = 10;
    _CS_POSIX_V6_LPBIG_OFFBIG_CFLAGS* = 11;
    _CS_POSIX_V6_LPBIG_OFFBIG_LDFLAGS* = 12;
    _CS_POSIX_V6_LPBIG_OFFBIG_LIBS* = 13;
    _CS_POSIX_V6_WIDTH_RESTRICTED_ENVS* = 14;
    _CS_V6_ENV* = -1;

  CONST
    SEEK_CUR* = 1;
    SEEK_END* = 2;
    SEEK_SET* = 0;

  CONST
    F_LOCK* = 1;
    F_TEST* = 3;
    F_TLOCK* = 2;
    F_ULOCK* = 0;

  CONST
    _PC_2_SYMLINKS* = 15;
    _PC_ALLOC_SIZE_MIN* = 16;
    _PC_ASYNC_IO* = 17;
    _PC_CHOWN_RESTRICTED* = 7;
    _PC_FILESIZEBITS* = 18;
    _PC_LINK_MAX* = 1;
    _PC_MAX_CANON* = 2;
    _PC_MAX_INPUT* = 3;
    _PC_NAME_MAX* = 4;
    _PC_NO_TRUNC* = 8;
    _PC_PATH_MAX* = 5;
    _PC_PIPE_BUF* = 6;
    _PC_PRIO_IO* = 19;
    _PC_REC_INCR_XFER_SIZE* = 20;
    _PC_REC_MAX_XFER_SIZE* = 21;
    _PC_REC_MIN_XFER_SIZE* = 22;
    _PC_REC_XFER_ALIGN* = 23;
    _PC_SYMLINK_MAX* = 24;
    _PC_SYNC_IO* = 25;
    _PC_TIMESTAMP_RESOLUTION* = -1;
    _PC_VDISABLE* = 9;

  CONST
    _SC_2_C_BIND* = 18;
    _SC_2_C_DEV* = 19;
    _SC_2_CHAR_TERM* = 20;
    _SC_2_FORT_DEV* = 21;
    _SC_2_FORT_RUN* = 22;
    _SC_2_LOCALEDEF* = 23;
    _SC_2_PBS* = 59;
    _SC_2_PBS_ACCOUNTING* = 60;
    _SC_2_PBS_CHECKPOINT* = 61;
    _SC_2_PBS_LOCATE* = 62;
    _SC_2_PBS_MESSAGE* = 63;
    _SC_2_PBS_TRACK* = 64;
    _SC_2_SW_DEV* = 24;
    _SC_2_UPE* = 25;
    _SC_2_VERSION* = 17;
    _SC_ADVISORY_INFO* = 65;
    _SC_AIO_LISTIO_MAX* = 42;
    _SC_AIO_MAX* = 43;
    _SC_AIO_PRIO_DELTA_MAX* = 44;
    _SC_ARG_MAX* = 1;
    _SC_ASYNCHRONOUS_IO* = 28;
    _SC_ATEXIT_MAX* = 107;
    _SC_BARRIERS* = 66;
    _SC_BC_BASE_MAX* = 9;
    _SC_BC_DIM_MAX* = 10;
    _SC_BC_SCALE_MAX* = 11;
    _SC_BC_STRING_MAX* = 12;
    _SC_CHILD_MAX* = 2;
    _SC_CLK_TCK* = 3;
    _SC_CLOCK_SELECTION* = 67;
    _SC_COLL_WEIGHTS_MAX* = 13;
    _SC_CPUTIME* = 68;
    _SC_DELAYTIMER_MAX* = 45;
    _SC_EXPR_NEST_MAX* = 14;
    _SC_FSYNC* = 38;
    _SC_GETGR_R_SIZE_MAX* = 70;
    _SC_GETPW_R_SIZE_MAX* = 71;
    _SC_HOST_NAME_MAX* = 72;
    _SC_IOV_MAX* = 56;
    _SC_IPV6* = 118;
    _SC_JOB_CONTROL* = 6;
    _SC_LINE_MAX* = 15;
    _SC_LOGIN_NAME_MAX* = 73;
    _SC_MAPPED_FILES* = 47;
    _SC_MEMLOCK* = 30;
    _SC_MEMLOCK_RANGE* = 31;
    _SC_MEMORY_PROTECTION* = 32;
    _SC_MESSAGE_PASSING* = 33;
    _SC_MONOTONIC_CLOCK* = 74;
    _SC_MQ_OPEN_MAX* = 46;
    _SC_MQ_PRIO_MAX* = 75;
    _SC_NGROUPS_MAX* = 4;
    _SC_OPEN_MAX* = 5;
    _SC_PAGE_SIZE* = 29;
    _SC_PAGESIZE* = 29;
    _SC_PRIORITIZED_IO* = 34;
    _SC_PRIORITY_SCHEDULING* = 35;
    _SC_RAW_SOCKETS* = 119;
    _SC_RE_DUP_MAX* = 16;
    _SC_READER_WRITER_LOCKS* = 76;
    _SC_REALTIME_SIGNALS* = 36;
    _SC_REGEXP* = 77;
    _SC_RTSIG_MAX* = 48;
    _SC_SAVED_IDS* = 7;
    _SC_SEM_NSEMS_MAX* = 49;
    _SC_SEM_VALUE_MAX* = 50;
    _SC_SEMAPHORES* = 37;
    _SC_SHARED_MEMORY_OBJECTS* = 39;
    _SC_SHELL* = 78;
    _SC_SIGQUEUE_MAX* = 51;
    _SC_SPAWN* = 79;
    _SC_SPIN_LOCKS* = 80;
    _SC_SPORADIC_SERVER* = 81;
    _SC_SS_REPL_MAX* = 126;
    _SC_STREAM_MAX* = 26;
    _SC_SYMLOOP_MAX* = 120;
    _SC_SYNCHRONIZED_IO* = 40;
    _SC_THREAD_ATTR_STACKADDR* = 82;
    _SC_THREAD_ATTR_STACKSIZE* = 83;
    _SC_THREAD_CPUTIME* = 84;
    _SC_THREAD_DESTRUCTOR_ITERATIONS* = 85;
    _SC_THREAD_KEYS_MAX* = 86;
    _SC_THREAD_PRIO_INHERIT* = 87;
    _SC_THREAD_PRIO_PROTECT* = 88;
    _SC_THREAD_PRIORITY_SCHEDULING* = 89;
    _SC_THREAD_PROCESS_SHARED* = 90;
    _SC_THREAD_ROBUST_PRIO_INHERIT* = -1;
    _SC_THREAD_ROBUST_PRIO_PROTECT* = -1;
    _SC_THREAD_SAFE_FUNCTIONS* = 91;
    _SC_THREAD_SPORADIC_SERVER* = 92;
    _SC_THREAD_STACK_MIN* = 93;
    _SC_THREAD_THREADS_MAX* = 94;
    _SC_THREADS* = 96;
    _SC_TIMEOUTS* = 95;
    _SC_TIMER_MAX* = 52;
    _SC_TIMERS* = 41;
    _SC_TRACE* = 97;
    _SC_TRACE_EVENT_FILTER* = 98;
    _SC_TRACE_EVENT_NAME_MAX* = 127;
    _SC_TRACE_INHERIT* = 99;
    _SC_TRACE_LOG* = 100;
    _SC_TRACE_NAME_MAX* = 128;
    _SC_TRACE_SYS_MAX* = 129;
    _SC_TRACE_USER_EVENT_MAX* = 130;
    _SC_TTY_NAME_MAX* = 101;
    _SC_TYPED_MEMORY_OBJECTS* = 102;
    _SC_TZNAME_MAX* = 27;
    _SC_V7_ILP32_OFF32* = -1;
    _SC_V7_ILP32_OFFBIG* = -1;
    _SC_V7_LP64_OFF64* = -1;
    _SC_V7_LPBIG_OFFBIG* = -1;
    _SC_V6_ILP32_OFF32* = 103;
    _SC_V6_ILP32_OFFBIG* = 104;
    _SC_V6_LP64_OFF64* = 105;
    _SC_V6_LPBIG_OFFBIG* = 106;
    _SC_VERSION* = 8;
    _SC_XOPEN_CRYPT* = 108;
    _SC_XOPEN_ENH_I18N* = 109;
    _SC_XOPEN_REALTIME* = 111;
    _SC_XOPEN_REALTIME_THREADS* = 112;
    _SC_XOPEN_SHM* = 113;
    _SC_XOPEN_STREAMS* = 114;
    _SC_XOPEN_UNIX* = 115;
    _SC_XOPEN_UUCP* = -1;
    _SC_XOPEN_VERSION* = 116;

  CONST
    STDERR_FILENO* = 2;
    STDIN_FILENO* = 0;
    STDOUT_FILENO* = 1;

  CONST
    _POSIX_VDISABLE* = 255;

  TYPE
    size_t* = PosixCsys_types.size_t;
    ssize_t* = PosixCsys_types.ssize_t;
    uid_t* = PosixCsys_types.uid_t;
    gid_t* = PosixCsys_types.gid_t;
    off_t* = PosixCsys_types.off_t;
    pid_t* = PosixCsys_types.pid_t;

  TYPE
    intptr_t* = INTEGER;

  PROCEDURE [ccall] access* (IN path: ARRAY [untagged] OF SHORTCHAR; amode: int): int;
  PROCEDURE [ccall] alarm* (seconds: unsigned): unsigned;
  PROCEDURE [ccall] chdir* (IN path: ARRAY [untagged] OF SHORTCHAR): int;
  PROCEDURE [ccall] chown* (IN path: ARRAY [untagged] OF SHORTCHAR; owner: uid_t; group: gid_t): int;
  PROCEDURE [ccall] close* (fd: int): int;
  PROCEDURE [ccall] confstr* (name: int; VAR buf: ARRAY [untagged] OF SHORTCHAR; len: size_t);
  PROCEDURE [ccall] crypt* (IN key, salt: ARRAY [untagged] OF SHORTCHAR);
  PROCEDURE [ccall] dup* (oldfd: int): int;
  PROCEDURE [ccall] dup2* (oldfd, newfd: int): int;
  PROCEDURE [ccall] _exit* (status: int);
  PROCEDURE [ccall] encrypt* (VAR block: ARRAY [untagged] 64 OF SHORTCHAR; edflag: int);
  PROCEDURE [ccall] execv* (IN path: ARRAY [untagged] OF SHORTCHAR; IN argv: ARRAY [untagged] OF POINTER TO ARRAY [untagged] OF SHORTCHAR): int;
  PROCEDURE [ccall] execve* (IN path: ARRAY [untagged] OF SHORTCHAR; IN argv, envp: ARRAY [untagged] OF POINTER TO ARRAY [untagged] OF SHORTCHAR): int;
  PROCEDURE [ccall] execvp* (IN file: ARRAY [untagged] OF SHORTCHAR; IN argv, envp: ARRAY [untagged] OF POINTER TO ARRAY [untagged] OF SHORTCHAR): int;
  PROCEDURE [ccall] faccessat* (fd: int; IN path: ARRAY [untagged] OF SHORTCHAR; amode, flag: int): int;
  PROCEDURE [ccall] fchdir* (fildes: int): int;
  PROCEDURE [ccall] fchown* (fildes: int; owner: uid_t; group: gid_t): int;
  PROCEDURE [ccall] fchownat* (fd: int; IN path: ARRAY [untagged] OF SHORTCHAR; owner: uid_t; group: gid_t; flag: int): int;
  PROCEDURE [ccall] fdatasync* (fildes: int): int;
  PROCEDURE [ccall] fexecve* (fd: int; IN argv, envp: ARRAY [untagged] OF POINTER TO ARRAY [untagged] OF SHORTCHAR): int;
  PROCEDURE [ccall] fork* (): pid_t;
  PROCEDURE [ccall] fpathconf* (fd, name: int): long;
  PROCEDURE [ccall] fsync* (fildes: int): int;
  PROCEDURE [ccall] ftruncate* (fildes: int; length: off_t): int;
  PROCEDURE [ccall] getcwd* (VAR [nil] buf: ARRAY [untagged] OF SHORTCHAR; size: size_t): POINTER TO ARRAY [untagged] OF SHORTCHAR;
  PROCEDURE [ccall] getegid* (): gid_t;
  PROCEDURE [ccall] geteuid* (): uid_t;
  PROCEDURE [ccall] getgid* (): gid_t;
  PROCEDURE [ccall] getgroups* (gidsetsize: int; VAR grouplist: ARRAY [untagged] OF gid_t): int;
  PROCEDURE [ccall] gethostid* (): long;
  PROCEDURE [ccall] gethostname* (VAR name: ARRAY [untagged] OF SHORTCHAR; namelen: size_t): int;
  PROCEDURE [ccall] getlogin* (): POINTER TO ARRAY [untagged] OF SHORTCHAR;
  PROCEDURE [ccall] getlogin_r* (VAR buf: ARRAY [untagged] OF SHORTCHAR; bufsize: size_t): int;
  PROCEDURE [ccall] getopt* (argc: int; IN argv: ARRAY [untagged] OF POINTER TO ARRAY [untagged] OF SHORTCHAR; IN optstring: ARRAY [untagged] OF SHORTCHAR): int;
  PROCEDURE [ccall] getpgid* (pid: pid_t): pid_t;
  PROCEDURE [ccall] getpgrp* (): pid_t;
  PROCEDURE [ccall] getpid* (): pid_t;
  PROCEDURE [ccall] getppid* (): pid_t;
  PROCEDURE [ccall] getsid* (): pid_t;
  PROCEDURE [ccall] getuid* (): uid_t;
  PROCEDURE [ccall] isatty* (fd: int): int;
  PROCEDURE [ccall] lchown* (IN path: ARRAY [untagged] OF SHORTCHAR; owner: uid_t; group: gid_t): int;
  PROCEDURE [ccall] link* (IN path1, path2: ARRAY [untagged] OF SHORTCHAR): int;
  PROCEDURE [ccall] linkat* (fd1: int; IN path1: ARRAY [untagged] OF SHORTCHAR; fd2: int; IN path2: ARRAY [untagged] OF SHORTCHAR; flag: int): int;
  PROCEDURE [ccall] lockf* (fd, cmd: int; len: off_t): int;
  PROCEDURE [ccall] lseek* (fildes: int; offset: off_t; whence: int): off_t;
  PROCEDURE [ccall] nice* (incr: int): int;
  PROCEDURE [ccall] pathconf* (IN path: ARRAY [untagged] OF SHORTCHAR; name: int): long;
  PROCEDURE [ccall] pause* (): int;
  PROCEDURE [ccall] pipe* (VAR fildes: ARRAY [untagged] 2 OF int): int;
  PROCEDURE [ccall] pread* (fildes: int; buf: PosixCtypes.Pvoid; nbyte: size_t; offset: off_t): ssize_t;
  PROCEDURE [ccall] pwrite* (fildes: int; buf: PosixCtypes.Pvoid; nbyte: size_t; offset: off_t): ssize_t;
  PROCEDURE [ccall] read* (fildes: int; buf: PosixCtypes.Pvoid; nbyte: size_t): ssize_t;
  PROCEDURE [ccall] readlink* (IN path: ARRAY [untagged] OF SHORTCHAR; VAR buf: ARRAY [untagged] OF SHORTCHAR; bufsize: size_t): ssize_t;
  PROCEDURE [ccall] readlinkat* (fd: int; IN path: ARRAY [untagged] OF SHORTCHAR; VAR buf: ARRAY [untagged] OF SHORTCHAR; bufsize: size_t): ssize_t;
  PROCEDURE [ccall] rmdir* (IN path: ARRAY [untagged] OF SHORTCHAR): int;
  PROCEDURE [ccall] setegid* (gid: gid_t): int;
  PROCEDURE [ccall] seteuid* (uid: uid_t): int;
  PROCEDURE [ccall] setgid* (gid: gid_t): int;
  PROCEDURE [ccall] setpgid* (pid, pgid: pid_t): int;
  PROCEDURE [ccall] setpgrp* (): pid_t;
  PROCEDURE [ccall] setregid* (rgid, egid: pid_t): int;
  PROCEDURE [ccall] setreuid* (ruid, euid: uid_t): int;
  PROCEDURE [ccall] setsid* (): pid_t;
  PROCEDURE [ccall] setuid* (uid: uid_t): int;
  PROCEDURE [ccall] sleep* (seconds: unsigned): unsigned;
  PROCEDURE [ccall] swab* (from, to: PosixCtypes.Pvoid; n: ssize_t);
  PROCEDURE [ccall] symlink* (IN path1, path2: ARRAY [untagged] OF SHORTCHAR): int;
  PROCEDURE [ccall] symlinkat* (IN path1: ARRAY [untagged] OF SHORTCHAR; fd: int; IN path2: ARRAY [untagged] OF SHORTCHAR): int;
  PROCEDURE [ccall] sync* ;
  PROCEDURE [ccall] sysconf* (name: int): long;
  PROCEDURE [ccall] tcgetpgrp* (fd: int): pid_t;
  PROCEDURE [ccall] tcsetpgrp* (fd: int; pgrp: pid_t): int;
  PROCEDURE [ccall] truncate* (IN path: ARRAY [untagged] OF SHORTCHAR; length: off_t): int;
  PROCEDURE [ccall] ttyname* (fd: int): POINTER TO ARRAY [untagged] OF SHORTCHAR;
  PROCEDURE [ccall] ttyname_r* (fd: int; VAR buf: ARRAY [untagged] OF SHORTCHAR; buflen: size_t): int;
  PROCEDURE [ccall] unlink* (IN path: ARRAY [untagged] OF SHORTCHAR): int;
  PROCEDURE [ccall] unlinkat* (fd: int; IN path: ARRAY [untagged] OF SHORTCHAR; flag: int): int;
  PROCEDURE [ccall] write* (fildes: int; buf: PosixCtypes.Pvoid; nbyte: size_t): int;

END PosixCunistd.
