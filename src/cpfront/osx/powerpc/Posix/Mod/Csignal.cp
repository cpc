MODULE PosixCsignal ['signal.h'];

  (* generated by genposix.sh, do not modify *)

  IMPORT SYSTEM, PosixCtypes, PosixCsys_types, PosixCtime;

  TYPE
    char* = PosixCtypes.char;
    signed_char* = PosixCtypes.signed_char;
    unsigned_char* = PosixCtypes.unsigned_char;
    short* = PosixCtypes.short;
    short_int* = PosixCtypes.short_int;
    signed_short* = PosixCtypes.signed_short;
    signed_short_int* = PosixCtypes.signed_short_int;
    unsigned_short* = PosixCtypes.unsigned_short;
    unsigned_short_int* = PosixCtypes.unsigned_short_int;
    int* = PosixCtypes.int;
    signed* = PosixCtypes.signed;
    signed_int* = PosixCtypes.signed_int;
    unsigned* = PosixCtypes.unsigned;
    unsigned_int* = PosixCtypes.unsigned_int;
    long* = PosixCtypes.long;
    long_int* = PosixCtypes.long_int;
    signed_long* = PosixCtypes.signed_long;
    signed_long_int* = PosixCtypes.signed_long_int;
    unsigned_long* = PosixCtypes.unsigned_long;
    unsigned_long_int* = PosixCtypes.unsigned_long_int;
    long_long* = PosixCtypes.long_long;
    long_long_int* = PosixCtypes.long_long_int;
    signed_long_long* = PosixCtypes.signed_long_long;
    signed_long_long_int* = PosixCtypes.signed_long_long_int;
    unsigned_long_long* = PosixCtypes.unsigned_long_long;
    unsigned_long_long_int* = PosixCtypes.unsigned_long_long_int;
    float* = PosixCtypes.float;
    double* = PosixCtypes.double;
    long_double* = PosixCtypes.long_double;

  CONST
    SIG_DFL* = 0;
    SIG_ERR* = -1;
    SIG_IGN* = 1;

  TYPE
    pthread_t* = PosixCsys_types.pthread_t;
    pthread_attr_t* = PosixCsys_types.pthread_attr_t;
    size_t* = PosixCsys_types.size_t;
    uid_t* = PosixCsys_types.uid_t;
    pid_t* = PosixCsys_types.pid_t;

  TYPE
    struct_timespec* = PosixCtime.struct_timespec;

  TYPE
    sig_atomic_t* = INTEGER;
    sigset_t* = INTEGER;

  TYPE
    Pstruct_sigevent* = POINTER TO struct_sigevent;
    struct_sigevent* ['struct sigevent'] = RECORD [noalign] (* 20 *)
      sigev_notify*: int; (* 0+4 *)
      sigev_signo*: int; (* 4+4 *)
      sigev_value*: union_sigval; (* 8+4 *)
      sigev_notify_function*: PROCEDURE [ccall] (x: union_sigval); (* 12+4 *)
    END;

  CONST
    SIGEV_NONE* = 0;
    SIGEV_SIGNAL* = 1;
    SIGEV_THREAD* = 3;

  TYPE
    Punion_sigval* = POINTER TO union_sigval;
    union_sigval* ['union sigval'] = RECORD [union] (* 4 *)
      sival_int*: int; (* 0+4 *)
      sival_ptr*: PosixCtypes.Pvoid; (* 0+4 *)
    END;

  CONST
    SIGABRT* = 6;
    SIGALRM* = 14;
    SIGBUS* = 10;
    SIGCHLD* = 20;
    SIGCONT* = 19;
    SIGFPE* = 8;
    SIGHUP* = 1;
    SIGILL* = 4;
    SIGINT* = 2;
    SIGKILL* = 9;
    SIGPIPE* = 13;
    SIGQUIT* = 3;
    SIGSEGV* = 11;
    SIGSTOP* = 17;
    SIGTERM* = 15;
    SIGTSTP* = 18;
    SIGTTIN* = 21;
    SIGTTOU* = 22;
    SIGUSR1* = 30;
    SIGUSR2* = 31;
    SIGSYS* = 12;
    SIGTRAP* = 5;
    SIGURG* = 16;
    SIGVTALRM* = 26;
    SIGXCPU* = 24;
    SIGXFSZ* = 25;

  TYPE
    P_struct_sigaction* = POINTER TO _struct_sigaction;
    _struct_sigaction* ['struct sigaction'] = RECORD [noalign] (* 12 *)
      sa_handler*: PROCEDURE [ccall] (sig: int); (* 0+4 *)
      sa_sigaction*: PROCEDURE [ccall] (sig: int; IN siginfo: siginfo_t; context: PosixCtypes.Pvoid); (* 0+4 *)
      sa_mask*: sigset_t; (* 4+4 *)
      sa_flags*: int; (* 8+4 *)
    END;

  TYPE
    Pstruct_sigaction* = POINTER TO struct_sigaction;
    struct_sigaction* ['struct sigaction'] = RECORD [noalign] (* 12 *)
      handler* ["/*handler"]: RECORD [union] (* 4 *)
        sa_handler* ["*/sa_handler"]: PROCEDURE [ccall] (sig: int); (* 0+4 *)
        sa_sigaction* ["*/sa_sigaction"]: PROCEDURE [ccall] (sig: int; IN siginfo: siginfo_t; context: PosixCtypes.Pvoid); (* 0+4 *)
      END; (* 0+4 *)
      sa_mask*: sigset_t; (* 4+4 *)
      sa_flags*: int; (* 8+4 *)
    END;

  CONST
    SIG_BLOCK* = 1;
    SIG_UNBLOCK* = 2;
    SIG_SETMASK* = 3;

  CONST
    SA_NOCLDSTOP* = 8;
    SA_ONSTACK* = 1;
    SA_RESETHAND* = 4;
    SA_RESTART* = 2;
    SA_SIGINFO* = 64;
    SA_NODEFER* = 16;
    SS_ONSTACK* = 1;
    SS_DISABLE* = 4;
    MINSIGSTKSZ* = 32768;
    SIGSTKSZ* = 131072;

  TYPE
    mcontext_t* = INTEGER;

  TYPE
    Pucontext_t* = POINTER TO ucontext_t;
    ucontext_t* ['ucontext_t'] = RECORD [noalign] (* 32 *)
      uc_sigmask*: sigset_t; (* 4+4 *)
      uc_stack*: stack_t; (* 8+12 *)
      uc_link*: Pucontext_t; (* 20+4 *)
      uc_mcontext*: mcontext_t; (* 28+4 *)
    END;

  TYPE
    Pstack_t* = POINTER TO stack_t;
    stack_t* ['stack_t'] = RECORD [noalign] (* 12 *)
      ss_sp*: PosixCtypes.Pvoid; (* 0+4 *)
      ss_size*: size_t; (* 4+4 *)
      ss_flags*: int; (* 8+4 *)
    END;

  TYPE
    Psiginfo_t* = POINTER TO siginfo_t;
    siginfo_t* ['siginfo_t'] = RECORD [noalign] (* 64 *)
      si_signo*: int; (* 0+4 *)
      si_errno*: int; (* 4+4 *)
      si_code*: int; (* 8+4 *)
      info* ["/*info"]: RECORD [union] (* 12 *)
        sigchld*: RECORD [noalign] (* 12 *)
          si_pid* ["*/si_pid"]: pid_t; (* 0+4 *)
          si_uid* ["*/si_uid"]: uid_t; (* 4+4 *)
          si_status* ["*/si_status"]: int; (* 8+4 *)
        END; (* 0+12 *)
        sigfpe*: RECORD [noalign] (* 4 *)
          si_addr* ["*/si_addr"]: PosixCtypes.Pvoid; (* 0+4 *)
        END; (* 12+4 *)
        sigsegv*: RECORD [noalign] (* 4 *)
          si_addr* ["*/si_addr"]: PosixCtypes.Pvoid; (* 0+4 *)
        END; (* 12+4 *)
        sigbus*: RECORD [noalign] (* 4 *)
          si_addr* ["*/si_addr"]: PosixCtypes.Pvoid; (* 0+4 *)
        END; (* 12+4 *)
        sigill*: RECORD [noalign] (* 4 *)
          si_addr* ["*/si_addr"]: PosixCtypes.Pvoid; (* 0+4 *)
        END; (* 12+4 *)
        other*: RECORD [noalign] (* 4 *)
          si_value* ["*/si_value"]: union_sigval; (* 0+4 *)
        END; (* 16+4 *)
      END; (* 12+12 *)
    END;

  CONST
    ILL_ILLOPC* = 1;
    ILL_ILLOPN* = 4;
    ILL_ILLADR* = 5;
    ILL_ILLTRP* = 2;
    ILL_PRVOPC* = 3;
    ILL_PRVREG* = 6;
    ILL_COPROC* = 7;
    ILL_BADSTK* = 8;

  CONST
    FPE_INTDIV* = 7;
    FPE_INTOVF* = 8;
    FPE_FLTDIV* = 1;
    FPE_FLTOVF* = 2;
    FPE_FLTUND* = 3;
    FPE_FLTRES* = 4;
    FPE_FLTINV* = 5;
    FPE_FLTSUB* = 6;

  CONST
    SEGV_MAPERR* = 1;
    SEGV_ACCERR* = 2;

  CONST
    BUS_ADRALN* = 1;
    BUS_ADRERR* = 2;
    BUS_OBJERR* = 3;

  CONST
    CLD_EXITED* = 1;
    CLD_KILLED* = 2;
    CLD_DUMPED* = 3;
    CLD_TRAPPED* = 4;
    CLD_STOPPED* = 5;
    CLD_CONTINUED* = 6;

  CONST
    SI_USER* = 65537;
    SI_QUEUE* = 65538;
    SI_TIMER* = 65539;
    SI_ASYNCIO* = 65540;
    SI_MESGQ* = 65541;

  PROCEDURE [ccall] kill* (pid: pid_t; sig: int): int;
  PROCEDURE [ccall] killpg* (pgrp, sig: int): int;
  PROCEDURE [ccall] psiginfo* (IN pinfo: siginfo_t; IN [nil] s: ARRAY [untagged] OF SHORTCHAR);
  PROCEDURE [ccall] psignal* (sig: int; IN [nil] s: ARRAY [untagged] OF SHORTCHAR);
  PROCEDURE [ccall] pthread_kill* (thread: pthread_t; sig: int): int;
  PROCEDURE [ccall] pthread_sigmask* (how: int; VAR [nil] set: sigset_t; VAR [nil] oldset: sigset_t): int;
  PROCEDURE [ccall] raise* (sig: int): int;
  PROCEDURE [ccall] sigaction* (sig: int; IN [nil] act: struct_sigaction; VAR [nil] oact: struct_sigaction): int;
  PROCEDURE [ccall] sigaddset* (VAR set: sigset_t; signum: int): int;
  PROCEDURE [ccall] sigaltstack* (IN [nil] ss: stack_t; VAR [nil] oss: stack_t): int;
  PROCEDURE [ccall] sigdelset* (VAR set: sigset_t; signum: int): int;
  PROCEDURE [ccall] sigemptyset* (VAR set: sigset_t): int;
  PROCEDURE [ccall] sigfillset* (VAR set: sigset_t): int;
  PROCEDURE [ccall] sigismember* (VAR set: sigset_t; signum: int): int;
  PROCEDURE [ccall] sigpending* (VAR set: sigset_t): int;
  PROCEDURE [ccall] sigprocmask* (how: int; VAR [nil] set: sigset_t; VAR [nil] oset: sigset_t): int;
  PROCEDURE [ccall] sigqueue* (pid: pid_t; sig: int; IN value: union_sigval): int;
  PROCEDURE [ccall] sigsuspend* (VAR sigmask: sigset_t): int;
  PROCEDURE [ccall] sigtimedwait* (VAR set: sigset_t; VAR [nil] info: siginfo_t; IN timeout: struct_timespec): int;
  PROCEDURE [ccall] sigwait* (VAR set: sigset_t; VAR sig: int): int;
  PROCEDURE [ccall] sigwaitinfo* (VAR set: sigset_t; VAR [nil] info: siginfo_t): int;

END PosixCsignal.
