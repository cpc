MODULE PosixCfcntl ['fcntl.h'];

  (* generated by genposix.sh, do not modify *)

  IMPORT SYSTEM, PosixCtypes, PosixCsys_types;

  TYPE
    char* = PosixCtypes.char;
    signed_char* = PosixCtypes.signed_char;
    unsigned_char* = PosixCtypes.unsigned_char;
    short* = PosixCtypes.short;
    short_int* = PosixCtypes.short_int;
    signed_short* = PosixCtypes.signed_short;
    signed_short_int* = PosixCtypes.signed_short_int;
    unsigned_short* = PosixCtypes.unsigned_short;
    unsigned_short_int* = PosixCtypes.unsigned_short_int;
    int* = PosixCtypes.int;
    signed* = PosixCtypes.signed;
    signed_int* = PosixCtypes.signed_int;
    unsigned* = PosixCtypes.unsigned;
    unsigned_int* = PosixCtypes.unsigned_int;
    long* = PosixCtypes.long;
    long_int* = PosixCtypes.long_int;
    signed_long* = PosixCtypes.signed_long;
    signed_long_int* = PosixCtypes.signed_long_int;
    unsigned_long* = PosixCtypes.unsigned_long;
    unsigned_long_int* = PosixCtypes.unsigned_long_int;
    long_long* = PosixCtypes.long_long;
    long_long_int* = PosixCtypes.long_long_int;
    signed_long_long* = PosixCtypes.signed_long_long;
    signed_long_long_int* = PosixCtypes.signed_long_long_int;
    unsigned_long_long* = PosixCtypes.unsigned_long_long;
    unsigned_long_long_int* = PosixCtypes.unsigned_long_long_int;
    float* = PosixCtypes.float;
    double* = PosixCtypes.double;
    long_double* = PosixCtypes.long_double;

  CONST
    F_DUPFD* = 0;
    F_DUPFD_CLOEXEC* = 14;
    F_GETFD* = 1;
    F_SETFD* = 2;
    F_GETFL* = 3;
    F_SETFL* = 4;
    F_GETLK* = 7;
    F_SETLK* = 8;
    F_SETLKW* = 9;
    F_GETOWN* = 5;
    F_SETOWN* = 6;

  CONST
    FD_CLOEXEC* = 1;

  CONST
    F_RDLCK* = 1;
    F_UNLCK* = 3;
    F_WRLCK* = 2;

  CONST
    SEEK_SET* = 0;
    SEEK_CUR* = 1;
    SEEK_END* = 2;

  CONST
    O_CLOEXEC* = 262144;
    O_CREAT* = 512;
    O_DIRECTORY* = 2097152;
    O_EXCL* = 2048;
    O_NOCTTY* = 32768;
    O_NOFOLLOW* = 1048576;
    O_TRUNC* = 1024;
    O_APPEND* = 8;
    O_DSYNC* = 8192;
    O_NONBLOCK* = 16384;
    O_RSYNC* = 8192;
    O_SYNC* = 8192;
    O_ACCMODE* = 3;
    O_RDONLY* = 0;
    O_RDWR* = 2;
    O_WRONLY* = 1;

  CONST
    AT_FDCWD* = -2;
    AT_EACCESS* = 1;
    AT_SYMLINK_NOFOLLOW* = 2;
    AT_SYMLINK_FOLLOW* = 4;
    AT_REMOVEDIR* = 8;

  CONST
    POSIX_FADV_DONTNEED* = 4;
    POSIX_FADV_NOREUSE* = 5;
    POSIX_FADV_NORMAL* = 0;
    POSIX_FADV_RANDOM* = 2;
    POSIX_FADV_SEQUENTIAL* = 1;
    POSIX_FADV_WILLNEED* = 3;

  TYPE
    Pstruct_flock* = POINTER TO struct_flock;
    struct_flock* ['struct flock'] = RECORD [noalign] (* 32 *)
      l_type*: short; (* 0+2 *)
      l_whence*: short; (* 2+2 *)
      l_start*: off_t; (* 8+8 *)
      l_len*: off_t; (* 16+8 *)
      l_pid*: pid_t; (* 24+4 *)
    END;

  TYPE
    mode_t* = PosixCsys_types.mode_t;
    off_t* = PosixCsys_types.off_t;
    pid_t* = PosixCsys_types.pid_t;

  PROCEDURE [ccall] creat* (IN pathname: ARRAY [untagged] OF SHORTCHAR; mode: mode_t): int;
  PROCEDURE [ccall] fcntl* (fildes, cmd, arg: int): int;
  PROCEDURE [ccall] open* (IN pathname: ARRAY [untagged] OF SHORTCHAR; flags: int; mode: mode_t): int;
  PROCEDURE [ccall] openat* (fddir: int; IN pathname: ARRAY [untagged] OF SHORTCHAR; flags: int; mode: mode_t): int;
  PROCEDURE [ccall] posix_fadvise* (fd: int; offset, len: off_t; advice: int): int;
  PROCEDURE [ccall] posix_fallocate* (fd: int; offset, len: off_t): int;

END PosixCfcntl.
