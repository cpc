MODULE PosixCmacro;

  IMPORT SYSTEM, PosixCerrno, PosixCsys_stat;

  PROCEDURE errno* (): PosixCerrno.int;
  BEGIN
    RETURN PosixCerrno.__errno()[0]
  END errno;

  PROCEDURE stat* (IN path: ARRAY [untagged] OF SHORTCHAR; VAR buf: PosixCsys_stat.struct_stat): PosixCsys_stat.int;
  BEGIN
    RETURN PosixCsys_stat.stat(path, buf)
  END stat;

END PosixCmacro.
