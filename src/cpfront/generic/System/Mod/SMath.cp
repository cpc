MODULE SMath;

  IMPORT SYSTEM;

  VAR
    eps, e: SHORTREAL;

  PROCEDURE [code] IncludeMATH "#include <math.h>";
  PROCEDURE [code] M_PI (): SHORTREAL "M_PI";
  PROCEDURE [code] sqrtf (x: SHORTREAL): SHORTREAL "sqrtf(x)";
  PROCEDURE [code] expf (x: SHORTREAL): SHORTREAL "expf(x)";
  PROCEDURE [code] logf (x: SHORTREAL): SHORTREAL "logf(x)";
  PROCEDURE [code] log10f (x: SHORTREAL): SHORTREAL "log10f(x)";
  PROCEDURE [code] powf (x, y: SHORTREAL): SHORTREAL "powf(x, y)";
  PROCEDURE [code] sinf (x: SHORTREAL): SHORTREAL "sinf(x)";
  PROCEDURE [code] cosf (x: SHORTREAL): SHORTREAL "cosf(x)";
  PROCEDURE [code] tanf (x: SHORTREAL): SHORTREAL "tanf(x)";
  PROCEDURE [code] asinf (x: SHORTREAL): SHORTREAL "asinf(x)";
  PROCEDURE [code] acosf (x: SHORTREAL): SHORTREAL "acosf(x)";
  PROCEDURE [code] atanf (x: SHORTREAL): SHORTREAL "atanf(x)";
  PROCEDURE [code] atan2f (y, x: SHORTREAL): SHORTREAL "atan2f(y, x)";
  PROCEDURE [code] sinhf (x: SHORTREAL): SHORTREAL "sinhf(x)";
  PROCEDURE [code] coshf (x: SHORTREAL): SHORTREAL "coshf(x)";
  PROCEDURE [code] tanhf (x: SHORTREAL): SHORTREAL "tanhf(x)";
  PROCEDURE [code] asinhf (x: SHORTREAL): SHORTREAL "asinhf(x)";
  PROCEDURE [code] acoshf (x: SHORTREAL): SHORTREAL "acoshf(x)";
  PROCEDURE [code] atanhf (x: SHORTREAL): SHORTREAL "atanhf(x)";
  PROCEDURE [code] floorf (x: SHORTREAL): SHORTREAL "floorf(x)";
  PROCEDURE [code] ceilf (x: SHORTREAL): SHORTREAL "ceilf(x)";
  PROCEDURE [code] roundf (x: SHORTREAL): SHORTREAL "roundf(x)";
  PROCEDURE [code] truncf (x: SHORTREAL): SHORTREAL "truncf(x)";
  PROCEDURE [code] copysignf (x, y: SHORTREAL): SHORTREAL "copysignf(x, y)";
  PROCEDURE [code] frexpf (x: SHORTREAL; OUT exp: INTEGER): SHORTREAL "frexpf(x, exp)";
  PROCEDURE [code] ldexpf (x: SHORTREAL; exp: INTEGER): SHORTREAL "ldexpf(x, exp)";

  PROCEDURE Pi* (): SHORTREAL;
  BEGIN
    RETURN M_PI()
  END Pi;

  PROCEDURE Eps* (): SHORTREAL;
  BEGIN
    RETURN eps
  END Eps;

  PROCEDURE Sqrt* (x: SHORTREAL): SHORTREAL;
  BEGIN
    RETURN sqrtf(x)
  END Sqrt;

  PROCEDURE Exp* (x: SHORTREAL): SHORTREAL;
  BEGIN
    RETURN expf(x)
  END Exp;

  PROCEDURE Ln* (x: SHORTREAL): SHORTREAL;
  BEGIN
    RETURN logf(x)
  END Ln;

  PROCEDURE Log* (x: SHORTREAL): SHORTREAL;
  BEGIN
    RETURN log10f(x)
  END Log;

  PROCEDURE Power* (x, y: SHORTREAL): SHORTREAL;
  BEGIN
    RETURN powf(x, y)
  END Power;

  PROCEDURE IntPower* (x: SHORTREAL; n: INTEGER): SHORTREAL;
    VAR y: SHORTREAL;
  BEGIN
    IF n = MIN(INTEGER) THEN RETURN IntPower(x, n + 1) / x END;
    y := 1.0;
    IF n < 0 THEN x := 1.0 / x; n := -n END;
    WHILE n > 0 DO
      IF ODD(n) THEN y := y * x; DEC(n)
      ELSE x := x * x; n := n DIV 2
      END
    END;
    RETURN y
  END IntPower;

  PROCEDURE Sin* (x: SHORTREAL): SHORTREAL;
  BEGIN
    RETURN sinf(x)
  END Sin;

  PROCEDURE Cos* (x: SHORTREAL): SHORTREAL;
  BEGIN
    RETURN cosf(x)
  END Cos;

  PROCEDURE Tan* (x: SHORTREAL): SHORTREAL;
  BEGIN
    RETURN tanf(x)
  END Tan;

  PROCEDURE SinCos* (x: SHORTREAL; OUT s, c: SHORTREAL);
  BEGIN
    s := sinf(x); c := cosf(x)
  END SinCos;

  PROCEDURE ArcSin* (x: SHORTREAL): SHORTREAL;
  BEGIN
    RETURN asinf(x)
  END ArcSin;

  PROCEDURE ArcCos* (x: SHORTREAL): SHORTREAL;
  BEGIN
    RETURN acosf(x)
  END ArcCos;

  PROCEDURE ArcTan* (x: SHORTREAL): SHORTREAL;
  BEGIN
    RETURN atanf(x)
  END ArcTan;

  PROCEDURE ArcTan2* (y, x: SHORTREAL): SHORTREAL;
  BEGIN
    RETURN atan2f(y, x)
  END ArcTan2;

  PROCEDURE Sinh* (x: SHORTREAL): SHORTREAL;
  BEGIN
    RETURN sinhf(x)
  END Sinh;

  PROCEDURE Cosh* (x: SHORTREAL): SHORTREAL;
  BEGIN
    RETURN coshf(x)
  END Cosh;

  PROCEDURE Tanh* (x: SHORTREAL): SHORTREAL;
  BEGIN
    RETURN tanhf(x)
  END Tanh;

  PROCEDURE ArcSinh* (x: SHORTREAL): SHORTREAL;
  BEGIN
    RETURN asinhf(x)
  END ArcSinh;

  PROCEDURE ArcCosh* (x: SHORTREAL): SHORTREAL;
  BEGIN
    RETURN acoshf(x)
  END ArcCosh;

  PROCEDURE ArcTanh* (x: SHORTREAL): SHORTREAL;
  BEGIN
    RETURN atanhf(x)
  END ArcTanh;

  PROCEDURE Floor* (x: SHORTREAL): SHORTREAL;
  BEGIN
    RETURN floorf(x)
  END Floor;

  PROCEDURE Ceiling* (x: SHORTREAL): SHORTREAL;
  BEGIN
    RETURN ceilf(x)
  END Ceiling;

  PROCEDURE Round* (x: SHORTREAL): SHORTREAL;
  BEGIN
    RETURN roundf(x)
  END Round;

  PROCEDURE Trunc* (x: SHORTREAL): SHORTREAL;
  BEGIN
    RETURN truncf(x)
  END Trunc;

  PROCEDURE Frac* (x: SHORTREAL): SHORTREAL;
  BEGIN
    IF x >= 0 THEN RETURN x - ENTIER(x)
    ELSE RETURN x + ENTIER(-x)
    END
  END Frac;

  PROCEDURE Mod1* (x: SHORTREAL): SHORTREAL;
  BEGIN
    RETURN x - ENTIER(x)
  END Mod1;

  PROCEDURE Sign* (x: SHORTREAL): SHORTREAL;
  BEGIN
    IF x > 0 THEN RETURN 1
    ELSIF x < 0 THEN RETURN -1
    ELSE RETURN x
    END
  END Sign;

  PROCEDURE SignBit* (x: SHORTREAL): BOOLEAN;
  BEGIN
    RETURN copysignf(1.0, x) > 0
  END SignBit;

  PROCEDURE CopySign* (x, y: SHORTREAL): SHORTREAL;
  BEGIN
    RETURN copysignf(x, y)
  END CopySign;

  PROCEDURE Mantissa* (x: SHORTREAL): SHORTREAL;
    VAR e: INTEGER;
  BEGIN
    RETURN frexpf(x, e);
  END Mantissa;

  PROCEDURE Exponent* (x: SHORTREAL): INTEGER;
    VAR m: SHORTREAL; e: INTEGER;
  BEGIN
    m := frexpf(x, e);
    RETURN e
  END Exponent;

  PROCEDURE Real* (m: SHORTREAL; e: INTEGER): SHORTREAL;
  BEGIN
    RETURN ldexpf(m, e)
  END Real;

BEGIN
  eps := 1.0E+0;
  e := 2.0E+0;
  WHILE e > 1.0E+0 DO
    eps := eps/2.0E+0;
    e := 1.0E+0 + eps
  END;
  eps := 2.0E+0 * eps
END SMath.
