MODULE Math;

  IMPORT SYSTEM;

  VAR
    eps, e: REAL;

  PROCEDURE [code] IncludeMATH "#include <math.h>";
  PROCEDURE [code] M_PI (): REAL "M_PI";
  PROCEDURE [code] sqrt (x: REAL): REAL "sqrt(x)";
  PROCEDURE [code] exp (x: REAL): REAL "exp(x)";
  PROCEDURE [code] log (x: REAL): REAL "log(x)";
  PROCEDURE [code] log10 (x: REAL): REAL "log10(x)";
  PROCEDURE [code] pow (x, y: REAL): REAL "pow(x, y)";
  PROCEDURE [code] sin (x: REAL): REAL "sin(x)";
  PROCEDURE [code] cos (x: REAL): REAL "cos(x)";
  PROCEDURE [code] tan (x: REAL): REAL "tan(x)";
  PROCEDURE [code] asin (x: REAL): REAL "asin(x)";
  PROCEDURE [code] acos (x: REAL): REAL "acos(x)";
  PROCEDURE [code] atan (x: REAL): REAL "atan(x)";
  PROCEDURE [code] atan2 (y, x: REAL): REAL "atan2(y, x)";
  PROCEDURE [code] sinh (x: REAL): REAL "sinh(x)";
  PROCEDURE [code] cosh (x: REAL): REAL "cosh(x)";
  PROCEDURE [code] tanh (x: REAL): REAL "tanh(x)";
  PROCEDURE [code] asinh (x: REAL): REAL "asinh(x)";
  PROCEDURE [code] acosh (x: REAL): REAL "acosh(x)";
  PROCEDURE [code] atanh (x: REAL): REAL "atanh(x)";
  PROCEDURE [code] floor (x: REAL): REAL "floor(x)";
  PROCEDURE [code] ceil (x: REAL): REAL "ceil(x)";
  PROCEDURE [code] round (x: REAL): REAL "round(x)";
  PROCEDURE [code] trunc (x: REAL): REAL "trunc(x)";
  PROCEDURE [code] copysign (x, y: REAL): REAL "copysign(x, y)";
  PROCEDURE [code] frexp (x: REAL; OUT exp: INTEGER): REAL "frexp(x, exp)";
  PROCEDURE [code] ldexp (x: REAL; exp: INTEGER): REAL "ldexp(x, exp)";

  PROCEDURE Pi* (): REAL;
  BEGIN
    RETURN M_PI()
  END Pi;

  PROCEDURE Eps* (): REAL;
  BEGIN
    RETURN eps
  END Eps;

  PROCEDURE Sqrt* (x: REAL): REAL;
  BEGIN
    RETURN sqrt(x)
  END Sqrt;

  PROCEDURE Exp* (x: REAL): REAL;
  BEGIN
    RETURN exp(x)
  END Exp;

  PROCEDURE Ln* (x: REAL): REAL;
  BEGIN
    RETURN log(x)
  END Ln;

  PROCEDURE Log* (x: REAL): REAL;
  BEGIN
    RETURN log10(x)
  END Log;

  PROCEDURE Power* (x, y: REAL): REAL;
  BEGIN
    RETURN pow(x, y)
  END Power;

  PROCEDURE IntPower* (x: REAL; n: INTEGER): REAL;
    VAR y: REAL;
  BEGIN
    IF n = MIN(INTEGER) THEN RETURN IntPower(x, n + 1) / x END;
    y := 1.0;
    IF n < 0 THEN x := 1.0 / x; n := -n END;
    WHILE n > 0 DO
      IF ODD(n) THEN y := y * x; DEC(n)
      ELSE x := x * x; n := n DIV 2
      END
    END;
    RETURN y
  END IntPower;

  PROCEDURE Sin* (x: REAL): REAL;
  BEGIN
    RETURN sin(x)
  END Sin;

  PROCEDURE Cos* (x: REAL): REAL;
  BEGIN
    RETURN cos(x)
  END Cos;

  PROCEDURE Tan* (x: REAL): REAL;
  BEGIN
    RETURN tan(x)
  END Tan;

  PROCEDURE SinCos* (x: REAL; OUT s, c: REAL);
  BEGIN
    s := sin(x); c := cos(x)
  END SinCos;

  PROCEDURE ArcSin* (x: REAL): REAL;
  BEGIN
    RETURN asin(x)
  END ArcSin;

  PROCEDURE ArcCos* (x: REAL): REAL;
  BEGIN
    RETURN acos(x)
  END ArcCos;

  PROCEDURE ArcTan* (x: REAL): REAL;
  BEGIN
    RETURN atan(x)
  END ArcTan;

  PROCEDURE ArcTan2* (y, x: REAL): REAL;
  BEGIN
    RETURN atan2(y, x)
  END ArcTan2;

  PROCEDURE Sinh* (x: REAL): REAL;
  BEGIN
    RETURN sinh(x)
  END Sinh;

  PROCEDURE Cosh* (x: REAL): REAL;
  BEGIN
    RETURN cosh(x)
  END Cosh;

  PROCEDURE Tanh* (x: REAL): REAL;
  BEGIN
    RETURN tanh(x)
  END Tanh;

  PROCEDURE ArcSinh* (x: REAL): REAL;
  BEGIN
    RETURN asinh(x)
  END ArcSinh;

  PROCEDURE ArcCosh* (x: REAL): REAL;
  BEGIN
    RETURN acosh(x)
  END ArcCosh;

  PROCEDURE ArcTanh* (x: REAL): REAL;
  BEGIN
    RETURN atanh(x)
  END ArcTanh;

  PROCEDURE Floor* (x: REAL): REAL;
  BEGIN
    RETURN floor(x)
  END Floor;

  PROCEDURE Ceiling* (x: REAL): REAL;
  BEGIN
    RETURN ceil(x)
  END Ceiling;

  PROCEDURE Round* (x: REAL): REAL;
  BEGIN
    RETURN round(x)
  END Round;

  PROCEDURE Trunc* (x: REAL): REAL;
  BEGIN
    RETURN trunc(x)
  END Trunc;

  PROCEDURE Frac* (x: REAL): REAL;
  BEGIN
    IF x >= 0 THEN RETURN x - ENTIER(x)
    ELSE RETURN x + ENTIER(-x)
    END
  END Frac;

  PROCEDURE Mod1* (x: REAL): REAL;
  BEGIN
    RETURN x - ENTIER(x)
  END Mod1;

  PROCEDURE Sign* (x: REAL): REAL;
  BEGIN
    IF x > 0 THEN RETURN 1
    ELSIF x < 0 THEN RETURN -1
    ELSE RETURN x
    END
  END Sign;

  PROCEDURE SignBit* (x: REAL): BOOLEAN;
  BEGIN
    RETURN copysign(1.0, x) > 0
  END SignBit;

  PROCEDURE CopySign* (x, y: REAL): REAL;
  BEGIN
    RETURN copysign(x, y)
  END CopySign;

  PROCEDURE Mantissa* (x: REAL): REAL;
    VAR e: INTEGER;
  BEGIN
    RETURN frexp(x, e);
  END Mantissa;

  PROCEDURE Exponent* (x: REAL): INTEGER;
    VAR m: REAL; e: INTEGER;
  BEGIN
    m := frexp(x, e);
    RETURN e
  END Exponent;

  PROCEDURE Real* (m: REAL; e: INTEGER): REAL;
  BEGIN
    RETURN ldexp(m, e)
  END Real;

BEGIN
  eps := 1.0E+0;
  e := 2.0E+0;
  WHILE e > 1.0E+0 DO
    eps := eps/2.0E+0;
    e := 1.0E+0 + eps
  END;
  eps := 2.0E+0 * eps
END Math.
