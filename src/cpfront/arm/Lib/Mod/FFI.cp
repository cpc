MODULE LibFFI ["ffi.h"];

  IMPORT SYSTEM, types := PosixCtypes, sys_types := PosixCsys_types;

  CONST
    SIZEOF_ARG* = 4;

  CONST (* abi *)
    FIRST_ABI* = 0;
    SYSV* = 1;
    VFP* = 2;
    LAST_ABI* = 2;
    (* DEFAULT_ABI* = SYSV; *)
    DEFAULT_ABI* = VFP; (* armhf *)

    (* stub *)
    STDCALL_WIN32* = -1; THISCALL* = -1; FASTCALL* = -1; MS_CDECL_WIN32* = -1;
    STDCALL_GENERIC* = -1; PASCAL* = -1; REGISTER* = -1;

  CONST (* status *)
    OK* = 0; BAD_TYPEDEF* = 1; BAD_ABI* = 2;

  CONST
    TYPE_VOID* = 0;
    TYPE_INT* = 1;
    TYPE_FLOAT* = 2;
    TYPE_DOUBLE* = 3;
    TYPE_LONGDOUBLE* = 4;
    TYPE_UINT8* = 5;
    TYPE_SINT8* = 6;
    TYPE_UINT16* = 7;
    TYPE_SINT16* = 8;
    TYPE_UINT32* = 9;
    TYPE_SINT32* = 10;
    TYPE_UINT64* = 11;
    TYPE_SINT64* = 12;
    TYPE_STRUCT* = 13;
    TYPE_POINTER* = 14;
    TYPE_COMPLEX* = 15;
    TYPE_LAST* = TYPE_COMPLEX;

  TYPE
    abi* ["ffi_abi"] = types.int; (* !!! enum *)
    status* ["ffi_status"] = types.int; (* !!! enum *)

    (* Ptype* = POINTER [untagged] TO type;
    PPtype* = POINTER [untagged] TO ARRAY [untagged] OF Ptype; *)
    type* ["ffi_type"] = RECORD [untagged]
      size*: sys_types.size_t;
      alignment*: types.unsigned_short;
      type*: types.unsigned_short;
      elements*: POINTER [untagged] TO ARRAY [untagged] OF POINTER TO type;
    END;

    (* Pcif* = POINTER TO cif; *)
    cif* ["ffi_cif"] = RECORD [untagged]
      abi*: abi;
      nargs*: types.unsigned;
      arg_type*: POINTER [untagged] TO ARRAY [untagged] OF POINTER TO type;
      rtype*: POINTER TO type;
      bytes*: types.unsigned;
      flags*: types.unsigned;
      (* --- ARM specific --- *)
      vfp_used: types.int;
      vfp_reg_free: types.unsigned_short;
      vfp_nargs: types.unsigned_short;
      vfp_args: ARRAY 16 OF types.char;
    END;

  VAR
    type_void- ["ffi_type_void"]: type;
    type_uint8- ["ffi_type_uint8"]: type;
    type_sint8- ["ffi_type_sint8"]: type;
    type_uint16- ["ffi_type_uint16"]: type;
    type_sint16- ["ffi_type_sint16"]: type;
    type_uint32- ["ffi_type_uint32"]: type;
    type_sint32- ["ffi_type_sint32"]: type;
    type_uint64- ["ffi_type_uint64"]: type;
    type_sint64- ["ffi_type_sint64"]: type;
    type_float- ["ffi_type_float"]: type;
    type_double- ["ffi_type_double"]: type;
    type_pointer- ["ffi_type_pointer"]: type;
    type_longdouble- ["ffi_type_longdouble"]: type;

    type_complex_single- ["ffi_type_complex_single"]: type;
    type_complex_double- ["ffi_type_complex_double"]: type;
    type_complex_longdouble- ["ffi_type_complex_longdouble"]: type;

  PROCEDURE [ccall] prep_cif* ["ffi_prep_cif"] (VAR c: cif; a: abi; nargs: types.unsigned_int; rtype: POINTER TO type; atypes: POINTER [untagged] TO ARRAY [untagged] OF POINTER TO type): status;
  PROCEDURE [ccall] prep_cif_var* ["ffi_prep_cif_var"] (VAR c: cif; a: abi; nfixedargs, ntotalargs: types.unsigned_int; rtype: POINTER TO type; atypes: POINTER [untagged] TO ARRAY [untagged] OF POINTER TO type): status;
  PROCEDURE [ccall] call* ["ffi_call"] (VAR c: cif; fn, rvalue, avalue: INTEGER);

END LibFFI.
