#! /bin/bash

set -e

abspath() {
  [[ "$1" == /* ]] && echo "$1" || echo "$(pwd)/$1"
}

###^^^^^^^^^^^^^^^^^^###
### Global variables ###
###__________________###

_this="$(dirname "$(abspath "$0")")"
_exec="make-all.sh"
_compiler=
_linker=

_cross=false
_bootstrap=false
_stages=2

_cpu=
_target=
_system=
_host_cpu=
_host_target=
_host_system=

export CPCFLAGS="$CPCFLAGS"
export CPLFLAGS="$CPLFLAGS"

###^^^^^^^^^^^###
### Functions ###
###___________###

usage() {
  echo "Usage: make-all.sh [options] cpu target os"
  echo "Options:"
  echo "    -c path           Path to compiler binary"
  echo "    -l path           Path to linker binary"
  echo "    -b                Bootstrap compiler (result in bootstrap/<cpu>-cpfront-<os>/)"
  echo "    -g num            Set max verify stages (0..2)"
#  echo "    -e                Cross compile (result in stage-cross)"
#  echo "    -u cpu            Host cpu"
#  echo "    -t target         Host compiler"
#  echo "    -s os             Host operaion system"
  echo "Processors:"
  echo "    486               Intel 486+"
  echo "    arm               ARM 32-bit"
  echo "    powerpc           PowerPC 32-bit"
  echo "Targets:"
  echo "    native            Native"
  echo "    cpfront           Generic C"
  echo "Operation systems:"
  echo "    linux             GNU/Linux"
  echo "    cygwin            Cygwin"
  echo "    osx               Mac OS X"
  echo "Environment variables:"
  echo "    CC                C compiler binary"
  echo "    CFLAGS            C compiler options"
  echo "    CPCFLAGS          CPC compiler options"
  echo "    CPLFLAGS          CPL linker options"
  exit 2
}

error() {
  echo "$_exec:" "$@"
  exit 1
}

make_stage() {
  local _dir="$1"
  local _this_cpu="$2"
  local _this_target="$3"
  local _this_system="$4"
  shift 4
  "$_this/make.sh" -o "$_dir" -c "$_compiler" -l "$_linker" "$@" "$_this_cpu" "$_this_target" "$_this_system"
  local _ext=
  if [ "$_host_system" = "cygwin" ]; then
    _ext=".exe"
  elif [ "$_host_system" = "osx" ]; then
    _ext=".out"
  fi
  if [ "$_target" = "cpfront" ]; then
    _compiler="$_dir/cpfront${_ext}"
    _linker=
  else
    if [ "$_cpu" = "486" ]; then
      _compiler="$_dir/cpc486${_ext}"
      _linker="$_dir/cpl486${_ext}"
    else
      error "unsupported cpu $_cpu"
    fi
  fi
}

make_verify_stage() {
  local _N="$1"
  shift
  if $_cross; then
    make_stage "$_this/stage$_N" "$@" "$_host_cpu" "$_host_target" "$_host_system"
  else
    make_stage "$_this/stage$_N" "$@" "$_cpu" "$_target" "$_system"
  fi
}

###^^^^^^^^^^^^^^^^^^^^^^^^^^^^^###
### Parse arguments and options ###
###_____________________________###

while getopts c:l:u:t:s:g:beh _name
do
  case "$_name" in
    c)  _compiler="$OPTARG" ;;
    l)  _linker="$OPTARG" ;;
    u)  _host_cpu="$OPTARG" ;;
    t)  _host_target="$OPTARG" ;;
    s)  _host_system="$OPTARG" ;;
    g)  _stages="$OPTARG" ;;
    b)  _bootstrap=true ;;
    e)  _cross=true ;;
    h|?)  usage ;;
  esac
done

if [ "$_stages" -lt "0" -o "$_stages" -gt "2" ]; then
  usage
fi

if [ "$(expr $# - $OPTIND + 1)" != "3" ]; then
  usage
fi

shift $(($OPTIND - 1))
_cpu="$1"
_target="$2"
_system="$3"

if [ -z "$_host_cpu" ]; then
  _host_cpu="$_cpu"
fi

if [ -z "$_host_target" ]; then
  _host_target="$_target"
fi

if [ -z "$_host_system" ]; then
  _host_system="$_system"
fi

###^^^^^^^^^^^^^^^^^^^^###
### Bootstrap compiler ###
###____________________###

if $_bootstrap; then
  echo "==> Bootstrap"
  _compiler=
  _linker=
  if $_cross; then
    make_stage "$_this/bootstrap/$_host_cpu-cpfront-$_host_system" "$_host_cpu" "cpfront" "$_host_system" -b
  else
    make_stage "$_this/bootstrap/$_host_cpu-cpfront-$_host_system" "$_cpu" "cpfront" "$_system" -b
  fi
else
  echo "==> Stage 0"
  make_verify_stage 0
fi

###^^^^^^^^^^^^^^^^^^###
### Recompile itself ###
###__________________###

if [ "1" -le "$_stages" ]; then
  echo "==> Stage 1"
  make_verify_stage 1
fi

if [ "2" -le "$_stages" ]; then
  echo "==> Stage 2"
  make_verify_stage 2
fi

###^^^^^^^^^^^^^^^###
### Cross compile ###
###_______________###

if $_cross; then
  echo "==> Cross compile"
  make_stage "$_this/stage-cross/$_cpu-$_target-$_system" "$_cpu" "$_target" "$_system"
fi

echo "==> Done"
