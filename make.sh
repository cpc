#! /bin/bash

set -e

abspath() {
  [[ "$1" == /* ]] && echo "$1" || echo "$(pwd)/$1"
}

###^^^^^^^^^^^^^^^^^^###
### Global variables ###
###__________________###

_exec="make.sh"
_this="$(dirname "$(abspath "$0")")"
_cpu=
_target=
_system=
_compiler=
_linker=
_docompile=true
_dolink=true
_out="$_this/bin"

_useposix=false

export CPCFLAGS="$CPCFLAGS"
export CPLFLAGS="$CPLFLAGS"

###^^^^^^^^^^^###
### Functions ###
###___________###

usage() {
  echo "Usage: make.sh [options] cpu target os"
  echo "Options:"
  echo "    -c path           Path to compiler binary"
  echo "    -l path           Path to linker binary"
  echo "    -o path           Path to output binaries"
  echo "    -b                Do not compile"
  echo "    -x                Do not link"
  echo "Processors:"
  echo "    486               Intel 486+"
  echo "    arm               ARM 32-bit"
  echo "    powerpc           PowerPC 32-bit"
  echo "Targets:"
  echo "    native            Native"
  echo "    cpfront           Generic C"
  echo "Operation systems:"
  echo "    linux             GNU/Linux"
  echo "    cygwin            Cygwin"
  echo "    osx               Mac OS X"
  echo "Environment variables:"
  echo "    CC                C compiler binary"
  echo "    CFLAGS            C compiler options"
  echo "    CPCFLAGS          CPC compiler options"
  echo "    CPLFLAGS          CPL linker options"
  exit 2
}

error() {
  echo "$_exec:" "$@"
  exit 1
}

copy_source() {
  for _src
  do
    if test -d "$_this/src/$_src"; then
      find "$_this/src/$_src" -mindepth 1 -maxdepth 1 -exec cp -r {} "$_out" \;
    fi
  done
}

native_compile() {
  "$_compiler" $CPCFLAGS -legacy "$@"
}

native_link() {
  if $_dolink; then
    local _outexe="$1";
    local _outsystem="$_system"
    if [ "$_system" = "cygwin" ]; then
      _outexe="${_outexe}.exe"
      _outsystem="win32"
    elif [ "$_system" = "osx" ]; then
      _outexe="${_outexe}.out"
    fi
    shift
    "$_linker" $CPLFALGS -os "$_outsystem" -kernel Kernel -main Kernel -legacycodedir . -o "$_outexe" "$@"
    fi
}

cpfront_import_list() {
  echo
  while [ "$1" != "" ]
  do
    echo -n "    $1"
    shift
    if ! [ -z "$1" ]; then
      echo ","
    fi
  done
}

cpfront_main_module() {
   local _name="$1"
   shift
   echo "MODULE ${_name};"
   echo
   echo "  IMPORT $(cpfront_import_list "$@");"
   echo
   echo "END ${_name}."
}

cpfront_compile() {
  "$_compiler" $CPCFLAGS -outcode CodeC -outsym SymC "$@"
}

cpfront_link() {
  local _main="$1"
  if $_docompile; then
    cpfront_main_module "$@" > "${_main}.cp"
    "$_compiler" $CPCFLAGS -outcode CodeC -outsym SymC -main "${_main}.cp"
  fi
  shift
  if $_dolink; then
    local _list=
    for _module in "$@" "${_main}"
    do
      _list="$_list ${_out}/CodeC/${_module}.c"
    done
    local _cc_cflags=
    case "$CC" in
      *gcc)  _cc_cflags="-std=c89 -Wno-int-conversion -Wno-int-to-pointer-cast -Wno-incompatible-pointer-types -Wno-implicit-function-declaration" ;;
      *gcc-4.2)  _cc_cflags="-std=c89 -Wno-int-to-pointer-cast -Wno-pointer-to-int-cast -Wno-implicit-function-declaration" ;;
      clang|clang-*)  _cc_cflags="-std=c89 -Wno-int-conversion -Wno-incompatible-pointer-types -Wno-logical-op-parentheses -Wno-bitwise-op-parentheses -Wno-pointer-sign -Wno-unused-value -Wno-return-type" ;;
      *tcc)  _cc_cflags="-std=c89 -w -fsigned-char" ;;
      *)  _cc_cflags="" ;;
    esac
    local _cpu_cflags=
    case "$_cpu" in
      486)  _cpu_cflags="-m32" ;;
      arm)  _cpu_cflags="" ;;
      powerpc)  _cpu_cflags="-m32" ;;
      *)  error "cpfront_link(): unsupported cpu $_cpu" ;;
    esac
    local _system_cflags=
    case "$_system" in
      cygwin)  _system_cflags="-liconv" ;;
      osx)  _system_cflags="-D_DARWIN_C_SOURCE -liconv" ;;
      *)  _system_cflags="" ;;
    esac
    local _out_exe="${_main}"
    case "$_system" in
      cygwin)  _out_exe="${_main}.exe" ;;
      osx)  _out_exe="${_main}.out" ;;
      *) ;;
    esac
    "$CC" -g -D_XOPEN_SOURCE=700 $_cc_cflags $_cpu_cflags $CFLAGS -o "${_out_exe}" -I "$_this/C" "$_this/C/SYSTEM.c" $_list -lm -ldl -lffi $_system_cflags
  fi
}

compile() {
  case "$_target" in
    native)  native_compile "$@" ;;
    cpfront)  cpfront_compile "$@" ;;
    *)  error "compile(): unknown target $_target" ;;
  esac
}

link() {
  case "$_target" in
    native)  native_link "$@" ;;
    cpfront)  cpfront_link "$@" ;;
    *)  error "link(): unknown target $_target" ;;
  esac
}

compile_all() {
  ###^^^^^^^^^^^^^^^^^^^^^^^^###
  ### Compile POSIX bindings ###
  ###________________________###

  if $_useposix; then
    compile Posix/Mod/Ctypes.cp \
      Posix/Mod/Csys_types.cp \
      Posix/Mod/Cstdlib.cp Posix/Mod/Cstdio.cp Posix/Mod/Cunistd.cp \
      Posix/Mod/Cdirent.cp Posix/Mod/Clocale.cp Posix/Mod/Ctime.cp \
      Posix/Mod/Csys_stat.cp Posix/Mod/Cfcntl.cp Posix/Mod/Cerrno.cp \
      Posix/Mod/Ciconv.cp Posix/Mod/Cwctype.cp Posix/Mod/Csys_mman.cp \
      Posix/Mod/Cdlfcn.cp Posix/Mod/Csignal.cp Posix/Mod/Csetjmp.cp \
      Posix/Mod/Clibgen.cp Posix/Mod/Csys_wait.cp \
      Posix/Mod/Cmacro.cp
    if [ "$_target" = "cpfront" ]; then
      compile Lib/Mod/FFI.cp
    fi
  fi

  ###^^^^^^^^^^^^^^^^^^^^^^^^^^^^###
  ### Compile BlackBox Framework ###
  ###____________________________###

  compile System/Mod/Int.odc
  if [ "$_target" = "native" ]; then
    compile System/Mod/Long.odc
    compile System/Mod/Math.odc System/Mod/SMath.odc
  else
    compile System/Mod/Math.cp System/Mod/SMath.cp
  fi
  compile System/Mod/Kernel.cp \
    System/Mod/Console.odc System/Mod/Files.odc System/Mod/Dates.odc \
    System/Mod/Log.odc System/Mod/Strings.odc System/Mod/Meta.odc \
    System/Mod/Services.odc System/Mod/Integers.odc

  if [ "$_target" = "native" ]; then
    mv -t System Code Sym
  fi

  ###^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^###
  ### Compile Linux Host subsystem ###
  ###______________________________###

  compile Host/Mod/Lang.cp Host/Mod/Dates.cp Host/Mod/Console.cp \
    Host/Mod/Files.cp

  ###^^^^^^^^^^^^^^^^^^^^^^^###
  ### Compile Dev subsystem ###
  ###_______________________###

  compile Dev/Mod/CPM.cp Dev/Mod/CPT.odc Dev/Mod/CPR.cp Dev/Mod/CPS.odc \
    Dev/Mod/CPB.odc Dev/Mod/CPP.odc Dev/Mod/CPE.odc Dev/Mod/CPH.odc \
    Dev/Mod/CPL486.odc Dev/Mod/CPC486.odc Dev/Mod/CPV486.odc

  ###^^^^^^^^^^^^^^^^^^^^^^^^###
  ### Compile Dev2 subsystem ###
  ###________________________###

  compile Dev2/Mod/LnkBase.odc Dev2/Mod/LnkChmod.odc Dev2/Mod/LnkLoad.odc \
    Dev2/Mod/LnkWriteElf.odc Dev2/Mod/LnkWriteElfStatic.odc \
    Dev2/Mod/LnkWritePe.odc

  ###^^^^^^^^^^^^^^^^^^^^^^^^^^^###
  ### Compile CPfront subsystem ###
  ###___________________________###

  compile CPfront/Mod/CPG.odc CPfront/Mod/CPC.odc CPfront/Mod/CPV.odc

  ###^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^###
  ### Compile bbdsw-specific modules ###
  ###________________________________###

  if [ "$_target" = "native" ]; then
    compile Dsw/Mod/Debug.odc
  fi

  compile Dsw/Mod/Documents.cp Dsw/Mod/Log.odc Dsw/Mod/Opts.cp Dsw/Mod/Procs.cp
  compile Dsw/Mod/HostProcs.cp

  ###^^^^^^^^^^^^^^^^^^^^^^^^^^^###
  ### Compile bbdsw executables ###
  ###___________________________###

  compile \
    Dsw/Mod/Compiler486Main.cp \
    Dsw/Mod/CompilerCPfrontMain.cp \
    Dsw/Mod/Linker486Main.cp \
    Dsw/Mod/MakeMain.cp
}

link_all() {
  local _debug_module=
  if [ "$_target" = "native" ]; then 
    _debug_module=DswDebug
  fi

  link cpc486 \
    PosixCtypes PosixCmacro \
    Kernel Console Files Dates Math Strings Services Log \
    HostLang HostConsole HostFiles HostDates DswLog $_debug_module \
    DevCPM DevCPT DevCPR DevCPS DevCPB DevCPP DevCPE DevCPH \
    DevCPL486 DevCPC486 DevCPV486 \
    DswDocuments DswCompiler486Main

  link cpl486 \
    PosixCtypes PosixCmacro \
    Kernel Console Files Math Strings Services Log \
    HostLang HostConsole HostFiles DswLog $_debug_module \
    Dev2LnkBase Dev2LnkChmod Dev2LnkLoad Dev2LnkWriteElf \
    Dev2LnkWriteElfStatic Dev2LnkWritePe \
    DswLinker486Main

  link cpfront \
    PosixCtypes PosixCmacro \
    Kernel Console Files Dates Math Strings Services Log \
    HostLang HostConsole HostFiles HostDates DswLog $_debug_module \
    DevCPM DevCPT DevCPR DevCPS DevCPB DevCPP DevCPE DevCPH \
    CPfrontCPG CPfrontCPC CPfrontCPV\
    DswDocuments DswCompilerCPfrontMain

  link cpmake \
    PosixCtypes PosixCmacro \
    Kernel Console Files Dates Math Strings Services Log \
    HostLang HostConsole HostFiles HostDates DswLog $_debug_module \
    DevCPM DevCPT DevCPR DevCPS \
    DswDocuments DswOpts DswProcs DswHostProcs DswMakeMain

  if $_dolink; then
    if [ "$_system" = "osx" ]; then
      chmod a+x cpc486.out cpl486.out cpfront.out
    else
      chmod a+x cpc486 cpl486 cpfront
    fi
  fi
}

###^^^^^^^^^^^^^^^^^^^^^^^^^^^^^###
### Parse arguments and options ###
###_____________________________###

while getopts c:l:o:bxh _name
do
  case "$_name" in
    c)  _compiler="$OPTARG" ;;
    l)  _linker="$OPTARG" ;;
    o)  _out="$OPTARG" ;;
    b)  _docompile=false ;;
    x)  _dolink=false ;;
    h|?)  usage ;;
  esac
done

if [ "$(expr $# - $OPTIND + 1)" != "3" ]; then
  usage
fi

shift $(($OPTIND - 1))
_cpu="$1"
_target="$2"
_system="$3"

if [ -z "$CC" ]; then
  export CC=cc
fi

###^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^###
### Check for supported cpu/target/os ###
###___________________________________###

case "$_cpu" in
  386|486|586|686)  _cpu=486 ;;
  arm|armv6|armv7)  _cpu=arm ;;
  powerpc|ppc|ppc32) _cpu=powerpc ;;
  "")  error "cpu not specified" ;;
  *)  error "unsupported cpu $_cpu" ;;
esac

case "$_target" in
  native) _target=native ;;
  cpfront|c)  _target=cpfront ;;
  "")  error "target not specified" ;;
  *)  error "unsupported target $_target" ;;
esac

case "$_system" in
  linux) _useposix=true ;;
  cygwin) _useposix=true ;;
  osx) _useposix=true ;;
  "") error "operation system not specified" ;;
  *) error "unsuported operation system $_system" ;;
esac

###^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^###
### Select default compiler if not specified ###
###__________________________________________###

if [ -z "$_compiler" ]; then
  case "$_target" in
    native)
      case "$_cpu" in
        486)  _compiler=cpc486 ;;
        *)  error "no standard compiler for cpu $_cpu" ;;
      esac
    ;;
    cpfront)  _compiler=cpfront ;;
    *) error "no standard compiler for target $_target" ;;
  esac
fi

if [ -z "$_linker" ]; then
  case "$_target" in
    native)
        case "$_cpu" in
          486)  _linker=cpl486 ;;
          *)  error "no standard linker for cpu $_cpu" ;;
        esac
      ;;
    cpfront)  _linker= ;;
    *) error "no standard linker for target $_target" ;;
  esac
fi

if $_docompile; then
  if command -v "$_compiler" > /dev/null; then
    _compiler="$(command -v "$_compiler")"
  else
    error "compiler not installed!"
  fi
fi

if $_dolink && [ "$_target" != "cpfront" ]; then
  if command -v "$_linker" > /dev/null; then
    _linker="$(command -v "$_linker")"
  else
    error "linker not installed!"
  fi
fi

###^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^###
### Copy sources for changed system ###
###_________________________________###

if $_docompile; then
  rm -rf -- "$_out"
fi

mkdir -p -- "$_out"
_out="$(abspath "$_out")"
copy_source "generic" "$_cpu"
if $_useposix; then
  copy_source "posix/generic" "posix/$_cpu"
fi
copy_source "$_system/generic" "$_system/$_cpu"
copy_source "$_target/generic" "$_target/$_cpu"
if $_useposix; then
  copy_source "$_target/posix/generic" "$_target/posix/$_cpu"
fi
copy_source "$_target/$_system/generic" "$_target/$_system/$_cpu"
cd "$_out"

###^^^^^^^^^^^^^^^###
### Build modules ###
###_______________###

if $_docompile; then
  compile_all
fi

###^^^^^^^^^^^^^^###
### Link modules ###
###______________###

link_all
