#! /bin/bash

set -e

abspath() {
  [[ "$1" == /* ]] && echo "$1" || echo "$(pwd)/$1"
}

###^^^^^^^^^^^^^^^^^^###
### Global variables ###
###__________________###

_exec="make-bootstrap.sh"
_this="$(dirname "$(abspath "$0")")"
_version="v0.3"
_sign=false

export CPCFLAGS="-no-use-time $CPCFLAGS"
export CPLFLAGS="$CPLFLAGS"

###^^^^^^^^^^^###
### Functions ###
###___________###

usage() {
  echo "Usage: make-bootstrap.sh [options]"
  echo "Options:"
  echo "    -v version        Set build version"
  echo "    -S                Sign build"
  echo "Environment variables:"
  echo "    CC                C compiler binary"
  echo "    CFLAGS            C compiler options"
  echo "    CPCFLAGS          CPC compiler options"
  echo "    CPLFLAGS          CPL linker options"
  exit 2
}

make_bootstrap() {
  local _cpu="$1"
  local _target="$2"
  local _system="$3"
  "$_this/make.sh" -x -o "$_this/bootstrap/$_cpu-$_target-$_system" \
    "$_cpu" "$_target" "$_system"
  find "$_this/bootstrap/$_cpu-$_target-$_system" -mindepth 1 -maxdepth 1 \
    -type d -name 'CodeC' -prune -o -exec rm -rf {} +
}

error() {
  echo "$_exec:" "$@"
  exit 1
}

###^^^^^^^^^^^^^^^###
### Parse options ###
###_______________###

while getopts v:Sh _name
do
  case "$_name" in
    v) _version="$OPTARG" ;;
    S) _sign=true ;;
    h|?) usage ;;
  esac
done

if [ -z "$_version" ] || echo "$_version" | grep " " > /dev/null; then
  error "version not specified or contain spaces"
fi

###^^^^^^^^^^^^^^^^^^^^###
### Prebuild C sources ###
###____________________###

rm -rf "$_this/bootstrap"
mkdir -p "$_this/bootstrap"
make_bootstrap 486 cpfront linux
#make_bootstrap 486 cpfront cygwin
#make_bootstrap arm cpfront linux
#make_bootstrap powerpc cpfront osx

###^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^###
### Hack: remove temp files from v0.1 ###
###___________________________________###

find "$_this/bootstrap" -type f -name '.new*' -delete

###^^^^^^^^^^^^^^^^^^^^^^###
### Package dist sources ###
###______________________###

rm -rf "$_this/cpc-$_version"
mkdir -p "$_this/cpc-$_version"
cp -r \
  "$_this/CHANGELOG" \
  "$_this/LICENSE" \
  "$_this/README" \
  "$_this/man" \
  "$_this/crux" \
  "$_this/make.sh" \
  "$_this/make-all.sh" \
  "$_this/make-bootstrap.sh" \
  "$_this/bootstrap" \
  "$_this/src" \
  "$_this/C" \
  "$_this/cpc-$_version"
tar czf "cpc-$_version.src.tar.gz" "cpc-$_version"

###^^^^^^^^^^^^^^^^###
### Make signature ###
###________________###

if $_sign; then
  gpg --yes --armor --detach-sig "cpc-$_version.src.tar.gz"
fi
